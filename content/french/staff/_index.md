---
title: Bénévoles, étudiants, membres du personnel
featureImage: images/single-blog/feature-image.jpg
postImage: images/skill/collage-h.jpg

---
## Dans les coulisses d'OCASI

### Direction et gestion

#### Directrice Générale
Debbie Douglas

#### Directrice Générale Adjointe
Eta Woldeab

#### Gérant, Finance et Administration
Nektarios Kikonyogo**

#### Gérant, IT et New Media
John Gilliam**
Alyssa McLeod*
Karen Scott*

#### Gérante, Développement de la capacité du secteur
Sajedeh Zahraei

#### Gérant, Ressources Humaines
Elaine Takata**

#### Manager, Entreprises productives
Elena Trapeznikova

### Membres du Personnel

Sidrah Ahmad*

Habbiba Ahmed

Sihem Ait Hammouda

Lionel Akagah**

Sahar Ashraf

Dena Badawi**

Michelle Ball

Soheil Baouji

Selina Basudde

Mariam Beker

Catherine Belokapov*

Paulina Bermeo

Stefany Brown

Amy Casipullai

Elise Craig**

Joselynn Crosby**

Manolli Ekra

Nira Elgueta**

Gregory Elward*

Anna Finch*

Miyuki Fukuma

Carmen Galvan*

Aini Gauhar*

Peggy Ho

Heffat Hussaini*

Sana Imran*

Sizwe Inkingi

Farheen Khan

Beverly Lawrence

Chenthoori Malankov*

Ali Mathis*

Leona McColeman

Emily Mooney*

Witta Nicoyishakiye*

Chavon Niles

Martha Orellana

Laura Osorio*

Tina Pahlevan*

Kandarp Patel*

Deborah Perne*

Margarita Pintin-Perez**

Theresa Polyakov

Gul-e Rana**

Ila Sethi

Brian Sharpe    

Laura Tachini

Tahira Tasneem*

Floraine Verheyde**

Marcos Vilela


### Internes

Mahmood Khan*


### Étudiants

Haby Bah*

Taskeen Nawab*

Sherene Whyte*





``(*)  Parti Pendant L’année – (**) Rejoint en Cours D’année``
