---
title: Technologie Et Nouveaux Médias
thumbnail: /images/portfolio/graphic-tech.jpg
postImage: /images/portfolio/graphic-tech-post.jpg
preview: Settlement.Org (Forum de discussion de Settlement.org), Etablissement.Org, NouveauxJeunes.Ca, AccueilOntario.ca, Système de gestion client OCASI
service: La Force Du Numérique
date: 2020-07-13T12:00:03.000+06:00
---
## La Force Du Numérique

### Settlement.Org

Depuis 2000, ce site web récipiendaire de prix constitue une ressource en ligne fiable pour les nouveaux arrivants et pour les organismes de service aux immigrants et aux réfugiés en Ontario.

Au cours de la dernière année, nous avons ajouté 18 articles et mis à jour plus de 53 afin de garantir un site pertinent et à jour. Le nouveau contenu porte sur les changements récents en matière d'immigration, les programmes de santé et les avantages sociaux relatifs à l'emploi. Services Near Me, notre outil cartographique interactif sur les services communautaires mis au point en collaboration avec Findhelp/211, s'avère une ressource toujours populaire parmi les utilisateurs. Nous poursuivons nos nombreux partenariats solides avec des organisations, des institutions et le gouvernement, dont Éducation juridique communautaire Ontario (CLÉO), Windmill Microlending et le Ministère de la Santé de l'Ontario, afin d'offrir aux nouveaux arrivants des contenus mis à jour de manière opportune.

« J'apprécie vos réponses et vos conseils supplémentaires dans la section sur l'anglais langue seconde. Je dois dire que c'est une excellente ressource, très utile pour les étudiants nouveaux arrivants. »

« Je suis arrivée au Canada à l'âge de 17 ans. Ma famille a pris l'habitude de chercher des renseignements sur le site Settlement.Org : ce fut d'une aide incroyable pour nous. »

Utilisateurs
+ 6,2 millions

18 nouveaux articles
53 articles mis à jour
384 événements annoncés

Twitter
+ 7 400 abonnés
+ 10 300 tweets

Facebook
+ 2 000 abonnés
+ 2 000 mentions « J'aime »


[use the links below with social media logos?]


https://www.facebook.com/Settlement.Org

https://twitter.com/Settlement_Org



### Forum de discussion Settlement.Org

Ce forum de discussion mené par des pairs permet aux utilisateurs de poser des questions à propos de la vie en Ontario et de partager leurs expériences avec les autres. Le forum renvoie constamment les utilisateurs vers le contenu de Settlement.Org.

Cette année, parmi les questions et préoccupations abordées le plus souvent on trouvait celle sur la carte d'assurance sociale et ce que l'on doit faire lorsqu'elle est perdue ou volée. Également et à l'instar des années récentes, le thème des voyages aux États-Unis en tant que résident permanent du Canada a été très populaire. Depuis mars 2020, nous avons constaté une tendance de nouveaux thèmes d'intérêt liés à la Prestation canadienne d'urgence, l'Assurance emploi et autres avantages financiers, tout comme les discussions concernant la manière de trouver un emploi dans le contexte de la pandémie de COVID-19.


Membres
+ 37 500

Thèmes
+ 3 600

Publications
+ 10 180


### Etablissement.Org

Etablissement.Org est un site web fiable offrant de l’information et aiguillage aux nouveaux arrivants francophones. Le site web couvre des thèmes dont l'immigration, l'emploi, l'éducation et la vie quotidienne.

Cette année, nous avons ajouté plus de 20 nouveaux articles et mis à jour plus d'une centaine. Parmi les contenus nouveaux et mis à jour on compte la santé, les finances personnelles, l'éducation et l'emploi. Les contenus les plus populaires depuis le début de la pandémie ont été l'immigration, la citoyenneté et l'Assurance emploi. La popularité du site continue d'augmenter. Le nombre total de nouveaux utilisateurs a dépassé les 728 000. Selon le Recensement de 2016, il y avait en Ontario 92 385 immigrants francophones, soit 15 % de la population francophone de la province.


« J’ai navigué un peu votre site, il y a une belle panoplie d'information. »

Nouveaux utilisateurs
+ 728 000

Lectures de pages
+ 1 199 400

Twitter
495 abonnées


Êtes-vous nouveau en Ontario?
https://settlement.org/outreach/order-promotional-materials/view?image=new-to-ontario-poster&type=png

OCASI sur le Web
https://www.youtube.com/watch?v=3f-lRijCOew

https://twitter.com/EtabSite


### NewYouth.ca / NouveauxJeunes.ca

Ces deux sites sont une source fiable pour les jeunes nouveaux arrivants en Ontario, leur offrant des renseignements sur les études, le travail, les droits et les services auxquels ils ont accès au Canada. Les sites ont été rénovés l'an passé et lancés en août 2019. Les mises à jour effectuées ont rendu la navigation plus aisée pour les utilisateurs, grâce à une nouvelle catégorisation des articles et à la possibilité de basculer entre le français et l'anglais, entre autres.

Cette année, nous avons ajouté des articles sur les droits des jeunes, l'éducation, l'immigration et d'autres sujets connexes. Après mars 2020, les articles les plus populaires ont été la location d'un logement et les droits des locataires, l'Assurance emploi et d'autres programmes d'assistance financière, fort probablement en lien avec la pandémie.


Utilisateurs
+ 96 510

Twitter
+ 2 750 abonnés

https://twitter.com/newcomeryouth


### Welcome Ontario / Accueil Ontario

WelcomeOntario / AccueilOntario soutient la réinstallation des réfugiés, offrant de l'information et des ressources pertinentes pour les réfugiés et pour ceux qui les aident à se réinstaller en Ontario. Le site aide à connecter les répondants privés et les travailleurs d'établissement et il habilite l'accès des réfugiés à des renseignements fiables.

À l'origine, OCASI a développé ces sites en 2015 comme un outil pour soutenir les initiatives de réinstallation de réfugiés syriens.

https://twitter.com/welcomeontario

https://www.facebook.com/WelcomeOntarioAccueilOntario/


### Système de gestion des clients d’OCASI

L'OCMS (OCASI Client Management System) est un système de gestion des données basé « dans le nuage » qui aide les organisations à s'assurer que leurs services soient offerts aux nouveaux arrivants de manière efficiente et efficace grâce à des outils qui font gagner du temps au travailleur et au client. Il s'agit d'une entreprise sociale d’OCASI qui a été développée par le secteur et pour le secteur et qui repose exclusivement sur les frais payés par les utilisateurs.

Cette année, nous avons ajouté de nouvelles fonctionnalités au système, dont de nouveaux rapports créés sur mesure pour les organismes et un outil de courriel pour faciliter l'aiguillage entre des organismes partenaires. Dans l'avenir, nous comptons offrir des options améliorées de formation en ligne.

Quotes & stats – use if/where relevant

« Help Desk répond toujours promptement et, d'habitude, m'aide à résoudre mes problèmes. Une excellente base de données ! »

« Dans l'ensemble, l'OCMS a été une superbe expérience pour notre organisme et nous apprécions le soutien et l'aide ! »

« Désormais, je peux compléter mon travail de manière efficace, merci. »

« Merci pour le dur travail effectué pour déménager l'OCMS à des meilleurs réseaux, serveurs, etc. Cela a fait une véritable différence qu'il est difficile de quantifier. »

Utilisateurs
2 200

Séances (depuis le début)
+ 2 486 000

Rapports générés
+ 4 000

https://ocasi.org/ocasi-client-management-system-ocms
