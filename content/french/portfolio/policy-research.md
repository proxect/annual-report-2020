---
title: Politiques et Recherche
thumbnail: /images/portfolio/graphic-policy-v.jpg
postImage: /images/portfolio/graphic-policy-post.jpg
preview: Relations gouvernementales, Relations sectorielles, Recherche, Politique et plaidoyer
service: Plaidoyer Pour Le Changement
date: 2020-07-13T12:00:01.000+06:00
---
## Plaidoyer Pour Le Changement

### Politiques publiques

Les priorités et préoccupations des organismes membres d'OCASI sont au centre de nos activités en matière de politiques. Nous nous concentrons principalement sur l'accès équitable aux services pour les réfugiés et pour tous les migrants, ainsi que sur des politiques d'immigration et d'asile justes.

Nous continuons de demander la retraite du Canada de l'Entente sur les tierces pays sûrs avec les États-Unis, l'accès à une audience en bonne et due forme pour tous les demandeurs d'asile et l'accès aux services de base, dont les soins de santé et le logement. Nous avons travaillé avec des partenaires et des alliés autour de la campagne de sensibilisation publique Allin4Canada avant les élections fédérales afin de contrer les attitudes négatives envers les réfugiés. L'accès et l'équité pour les étudiants internationaux a été une priorité clé dans nos démarches de plaidoyer en réponse aux préoccupations de nos organismes membres. En tant que membre de Colour of Poverty – Colour of Change, OCASI a demandé à tous les paliers de gouvernement d'adopter une recollection de données désagrégées sur la base de l'identification raciale afin de mieux comprendre et répondre à l'impact des inégalités raciales systémiques. Sur le plan de l'Ontario, nos priorités ont compris l'appel à une augmentation du salaire minimum et à la mise en place de meilleures protections pour les travailleurs, de services juridiques pour les réfugiés et les immigrants et de soins de santé pour tous.

### Recherche

Nous avons effectué une évaluation de la School for Social Justice (SSJ) d'OCASI, un institut de formation permettant à nos organismes membres de rehausser leurs capacités en matière d'analyse sociale, d'organisation communautaire et de plaidoyer pour la justice sociale et économique. L'évaluation a confirmé que la SSJ est une ressource valorisée et nécessaire pour nos organismes membres. Cependant, elle ne pourrait être durable sans un financement continu et le programme est en pause pour l'instant.

Nous avons collaboré avec des partenaires et des alliés autour d'un nombre de projets de recherche répondant aux priorités d'OCASI, dont le partenariat Immigration et résilience en milieu urbain, financé par le CRSH. Nous avons également collaboré avec le projet Youth Sexual Health and HIV/STI Prevention in Middle Eastern and North African Communities in Ontario, entre autres.  

---

### + 7 300 abonnés sur Twitter

> « Merci à OCASI pour son leadership. »
