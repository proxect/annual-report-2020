---
title: Accès Et Équité
thumbnail: /images/portfolio/graphic-access-v.jpg
postImage: /images/portfolio/graphic-access-post.jpg
preview: Initiatives d’espaces positifs, Initiative d’accessibilité, Promotion de la Santé mentale, Campagne pour les immigrants, les communautés de réfugiés, les voisins, les familles et les amis
service: Un Secteur Plus Équitable
date: 2020-07-13T12:00:04.000+06:00
---
## Un Secteur Plus Équitable

### L'Initiative pour des Espaces positifs

L'Initiative pour des Espaces positifs soutient le secteur au service des immigrants et des réfugiés afin qu'il puisse offrir des services plus efficaces aux nouveaux arrivants LGBTQIA+ (lesbiennes, gais, bisexuels, trans, bi-spirituels, allosexuels, en questionnement, intersexuels, asexuels, pan-sexuels, genderqueer, etc.). Nous avons continué à offrir de la formation aux organismes du secteur en vue d'une plus grande capacité et nous les avons soutenus dans leur cheminement vers la désignation comme des espaces positifs. Grâce à nos champions régionaux, nous avons connu un plus haut niveau d'activité dans nos réseaux régionaux, avec 9 événements tenus en Ontario, dont le lancement d'une Clinique d'Aide juridique à Cambridge, une pièce de théâtre sur des questions LGBTQIA+ à St. Catharines et un pique-nique pour les nouveaux arrivants LGBTQIA+ à London. En réponse à une grande demande, nous avons créé 60 ressources interactives qui s'ajoutent à nos outils d'apprentissage.

De concert avec l'Initiative pour l'Accessibilité, nous avons tenu des tables rondes bilingues sur les manières de surmonter les obstacles à la prestation de services qui découlent des biais et de la stigmatisation au sein du secteur. Nous avons également collaboré dans le recrutement d'alliés pour l'Accès et l'équité qui agiraient à titre de champions pour les deux initiatives et aideraient à bâtir des réseaux plus forts afin de rendre nos espaces plus positifs, sécuritaires, accessibles et inclusifs pour les nouveaux arrivants LGBTQIA+ et/ou ceux ayant des handicaps visibles ou invisibles.

http://positivespaces.ca/


60 nouvelles ressources interactives

32 ateliers de formation

+ 600 participants formés

+ 900 participants formés en ligne



« Les vidéos sont très utiles en ce qu'elles apportent une nouvelle perspective. Elles servent à 'humaniser' tout ce que nous sommes en train d'apprendre et je crois qu'elles ont un plus grand impact auprès des gens qui ont plus de difficulté à aborder ces sujets. Au moins, je l'espère. »

« Les sujets abordés dans cette formation étaient très intéressants car nous travaillons avec des adolescents qui, potentiellement, pourraient vivre ces situations et je voudrais que tous mes collègues/employés puissent être en mesure de venir en aide à une personne qui vit quelque chose de semblable. »

« J'ai apprécié l'opportunité de discuter de questions découlant de différents droits de la personne apparemment en conflit, puisque ce sujet est parfois évité au sein de mon milieu de travail ou considéré comme étant 'impossible'. J'apprécie aussi les outils et l'occasion d'évaluer mon propre milieu de travail et d'identifier des plans d'action pour améliorer les services. »


### L'Initiative pour l'Accessibilité

Cette initiative bilingue soutient les travailleurs au service des immigrants et des réfugiés voulant acquérir de nouvelles habiletés et connaissances pour offrir des services aux immigrants et aux réfugiés ayant des handicaps visibles ou invisibles. La formation et les ressources fournies par l'initiative aident les travailleurs à gagner une meilleure compréhension des lois relatives aux droits des personnes ayant un handicap, ainsi que la relation entre le handicap, la race, le statut d'immigrant ou de réfugié et d'autres facteurs de marginalisation. Nous avons tenu des tables rondes avec des organismes du secteur et nous avons offert de la formation dans un éventail de formats : en personne, en ligne (avec animation ou autodirigé).

Nous avons collaboré avec l'Initiative pour les Espaces positifs d'OCASI autour d'une table ronde conjointe à propos des biais et de la stigmatisation. Nous avons également collaboré au recrutement et à la formation d'alliés qui joueraient le rôle de champions des deux initiatives.

https://ocasi.org/accessibility-program

Boîte à outils pour l'Accessibilité :
https://ocasi.org/sites/default/files/accessibility-kit_0.pdf


+ 1 100 visiteurs au site web

2 nouvelles ressources bilingues

23 ateliers de formation bilingues

+ 320 participants

14 webinaires bilingues

+ 320 participants

+ 200 apprenants en ligne

« La formation a été très utile car je n'étais pas si familiarisé avec les lois concernant les handicaps en Ontario et avec ce que l'on peut faire pour aider les nouveaux arrivants ayant des handicaps. »

« Suivant un modèle interactif, les informations partagées étaient pertinentes pour mon poste (travailleuse d’établissement) et je pourrai les appliquer dans mon travail de tous les jours. »

« Cela m’a permis de me mettre en situation et dans la peau d’une personne ayant un handicap, les difficultés auxquelles ces personnes font face. »

« Très intéressant. J’aimerais refaire l’atelier mais plus en profondeur. Merci à OCASI pour les formations en français. »



### La promotion de la santé mentale

Par l'entremise de collaborations et de partenariats, cette initiative rehausse la capacité des organisations au service des immigrants et des réfugiés en vue de la promotion de la santé mentale et de répondre à des questions relatives parmi les populations de nouveaux arrivants. Elle prend appui sur un partenariat entre le secteur au service des immigrants et des réfugiés et les secteurs des soins de santé primaires et de la santé mentale.

Nous avons collaboré avec nos partenaires afin de développer un modèle de service intégré, sur la base d'une analyse environnementale et d'une évaluation des besoins complétées l'an passé. Nous avons travaillé avec Rexdale Women's Centre et avec Polycultural Immigrant and Community Services pour mettre à l'essai le modèle. Nous avons tenu un événement de deux jours très bien reçu pour l'échange de connaissances avec la participation d'organisations des milieux des soins de santé primaires, de la santé mentale et des services aux immigrants et aux réfugiés. Nous avons continué à offrir des formations pour le rehaussement des capacités aux travailleurs servant les immigrants et les réfugiés sur des sujets telle l'aide initiale en matière de santé mentale et la réduction de la stigmatisation de la santé mentale dans le secteur.


« [La formation] a ouvert mes yeux, rehaussant ma compréhension de l'importance de prendre soin de moi-même et de reconnaître le moment de demander de l'aide ou du soutien. »

« La formation a mis en perspective l'importance d'être bien mentalement dans le milieu de travail et, surtout, de ne pas oublier son propre bien-être. »


https://ocasi.org/mental-health-promotion

- Guide sur le traumatisme et la violence (en anglais) :
https://ocasi.org/sites/default/files/tvia-guide-english-online.pdf

Guide sur le traumatisme et la violence (en français) :
 https://ocasi.org/sites/default/files/tvia-guide-french-v3-2020.pdf


 + 175 participants aux ateliers

 « Merci de nous avoir offert une formation si merveilleuse, très utile pour moi en tant que travailleuse de première ligne. Les besoins des individus ayant des soucis de santé mentale sont tellement importants. »

 « Je serai en mesure de partager tout cela avec les jeunes avec lesquels je travaille et je serai pour eux une personne en qui ils peuvent avoir confiance. »

 « J'ai apprécié qu'on aborde la promotion de la santé mentale auprès des membres du personnel ainsi que le leadership en matière de santé mentale dans le milieu de travail. »


### La Campagne voisin-es, ami-es et familles : communautés immigrantes et réfugiées

Cette initiative rehausse les capacités communautaires pour que les témoins puissent intervenir lors de situations de violence domestique au sein des communautés immigrantes et réfugiées. Nous avons formé et agi à titre de mentors auprès de 14 Pairs champions. Le modèle a été reconnu comme pratique prometteuse par Voies vers la prospérité, l'alliance pancanadienne de partenaires universitaires, communautaires et gouvernementaux.

Nous avons numérisé nos Cartes de scénarios et nous les avons distribuées, tout comme d'autres matériels éducatifs, parmi les organismes servant les immigrants et les réfugiés et un éventail d'autres réseaux qui interagissent avec des nouveaux arrivants. Les nouvelles cartes numérisées ont été utilisées par les Pairs champions à des fins d'information et d'éducation dans le cadre d'événements communautaires, ce qui a eu un impact significatif tout particulièrement au sein de communautés ayant peu d'occasions de tenir des discussions sécuritaires à propos de la violence domestique et basée sur le genre. La numérisation de nos outils a aidé à doubler notre présence dans les médias sociaux.


https://www.immigrantandrefugeenff.ca/


14 Pairs champions formés

+ 300 organisations impliquées dans la campagne

28 ateliers de formation

1 100 participants aux formations

+ 4 500 visiteurs au site web de la campagne

+ 74 000 impressions sur les médias sociaux

« J'ai été très heureuse et satisfaite d'accomplir mon devoir de connecter la communauté avec ces ressources tellement nécessaires. J'ai hâte de remplir mon rôle comme Pair championne dans les années à venir, afin de maintenir et renforcer le lien que j'ai tissé avec la communauté au cours de ces événements. »

« Cette année m'a apporté de nouvelles occasions de tisser des liens avec plus de gens au sein du secteur de la lutte à la violence faite aux femmes et avec des membres de la communauté en tant que Pair champion. Ce rôle m'a habilité pour travailler avec des partenaires au sein de la communauté pour lutter contre la violence domestique. J'ai continué à apprendre des personnes ayant subi une telle violence et des membres de la communauté grâce au programme hebdomadaire de cuisine. »


### Bâtir la capacité de leadership pour répondre à la violence basée sur le genre affectant les femmes sans statut, réfugiées et immigrantes partout au Canada

Dans le cadre de cette initiative d'envergure pancanadienne, OCASI a établi des partenariats avec des organismes à but non lucratif partout au pays en vue de rehausser la capacité de leadership des femmes ayant un statut d’immigration précaire afin de répondre à la violence basée sur le genre. Les partenaires incluent : New Brunswick Multicultural Council, Immigrant Women Services Ottawa, Barbra Schlifer Commemorative Clinic, Rights of Non-Status Women Network, Sexual Assault Center of Edmonton, Islamic Family Services Association et MOSAIC.

Nous avons complété une analyse environnementale et une évaluation des besoins abordant les soutiens et services actuellement offerts dans le cadre de modèles de plaidoyer communautaire et menés par les personnes ayant subi une telle violence, utilisant une recension de la littérature, des consultations auprès des parties intéressées, un sondage, des groupes de discussion et des entrevues. Nous avons collaboré avec nos partenaires pour concevoir un Blueprint in Progress (Plan en développement) présentant des pratiques prometteuses reposant sur des pairs champions et des réseaux de plaidoyer communautaire. Afin de soutenir les prestataires de services, nous avons offert des webinaires et de cours de formation en ligne.

Nous entendons lancer une série de tables rondes virtuelles la prochaine année fiscale afin de rassembler des activistes communautaires, des leaders et des travailleuses du secteur ainsi que des femmes au statut migratoire précaire afin de concevoir des stratégies pour répondre à la violence basée sur le genre. Dans le contexte de la pandémie, ces tables rondes aideront à trouver des manières de mitiger l'impact de la COVID-19 sur cette partie de la population.

https://ocasi.org/gender-based-violence

+ 270 questionnaires d'évaluation de besoins remplis

+ 70 parties intéressées consultées

+ 100 personnes participantes aux formations

« Merci pour cette excellente présentation et d'avoir partagé le merveilleux travail en marche partout au Canada ! » – Participante à une table ronde

« Merci pour cette séance si intéressante et créative. Les œuvres d'art utilisées ont été géniales et les définitions de leadership partagées par les participantes ont été parmi les éléments que j'ai le plus appréciés. Il est si important de travailler de concert. » – Participante à une table ronde

« Excellente discussion et excellentes contributions lors de ce travail collectif. Merci pour cette opportunité et merci aux présentateurs. » – Participante à une table ronde
