---
title: Services Administratifs
thumbnail: /images/portfolio/graphic-office.jpg
postImage: /images/portfolio/graphic-office-post.jpg
preview: Le personnel administratif et de support technique d'OCASI fournit les fondements nécessaires pour soutenir toutes nos activités de service.
service: Des Fondements Forts
date: 2020-07-13T12:00:06.000+06:00
---
## Des Fondements Forts

Le personnel administratif et de support technique d'OCASI fournit les fondements nécessaires pour soutenir toutes nos activités de service.

### Les TI et la conception graphique

Cette année, nous avons migré tous nos sites de Drupal 7 vers Drupal 8, ayant consacré de longues heures à résoudre avec succès des nombreux défis techniques. Ainsi, nous pourrons utiliser les meilleures pratiques d'infrastructure et les dernières tendances de la technologie pendant des années à venir. Désormais, Drupal 8 nous aide à renforcer certaines fonctionnalités dont des composantes multilingues ainsi qu'une infrastructure nous permettant de connecter nos sites sur d'autres applications. Nous avons apporté des mises à jour au design de certains sites web, dont celui de l'Initiative pour l'Accessibilité, afin de les rendre plus accessibles et faciles à utiliser. Le fait de compter sur une expertise interne en matière de conception graphique nous permet d'avoir plus d'impact grâce au développement de formats visuellement engageants pour nos ressources et à la production d'outils graphiques pour les médias sociaux.

Nos activités permanentes ont compris la surveillance et la mise à jour des sites web, la mise à l'essai de plusieurs applications et l'installation et configuration de mises à jour en vue d'une sécurité accrue. Outre le travail technique quotidien et les activités de migration des sites, nous sommes restés à jour quant aux technologies émergentes afin de les mettre à contribution dans la mesure du possible et d'évaluer et de planifier stratégiquement les services qui pourraient bénéficier d'innovations, de sorte à augmenter notre impact.


### Administration

Nous avons continué à accomplir nos tâches administratives aussi efficacement que possible afin d'assurer le fonctionnement fluide de l'organisation grâce à la gestion financière, la comptabilité, le respect des normes et la gestion du bureau, entre autres. Depuis l'enclenchement de la pandémie, nous continuons à faire migrer la plupart de nos activités administratives vers un format en ligne.
