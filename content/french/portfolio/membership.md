---
title: Membriété
thumbnail: /images/portfolio/graphic-membership-v.jpg
postImage: /images/portfolio/graphic-membership-post.jpg
preview: Francophone Committee, Women’s Caucus
service: Impact collectif
date: 2020-07-13T12:00:05.000+06:00
---
## Impact collectif
### Comité francophone
Ce Comité conseille OCASI à propos des priorités des organismes francophones et en matière de services pour les réfugiés et les immigrants francophones. Il est formé des 19 organismes membres francophones d'OCASI et il est présidé par le représentant francophone au Conseil d'administration. Le Comité a plaidé avec succès auprès du gouvernement fédéral en vue de la mise en place d'un kiosque francophone à l'Aéroport International de Toronto pour aider les nouveaux arrivants francophones.

Le Comité espère que cette initiative soit reproduite dans d'autres collectivités et qu'on élargisse la portée et l'échelle de l'offre d'information en français avant l'arrivée au Canada. Son influence a aidé à faire en sorte que l'Ontario s'engage à assurer des niveaux annuels d'immigration francophone de 5%.

Les membres du Comité collaborent activement dans le cadre de la pandémie afin d'assurer que les immigrants francophones reçoivent le soutien dont ils ont besoin.

#### Membres du comité francophone d'OCASI

- Actions Interculturelles Canada
- Association Canadienne-Francaise de L'Ontario Conseil Regional Des Milles-Illes (ACFOMI)
- Auberge Francophone
- Centre communautaire francophone Windsor-Essex-Kent
- Centre communautaire régional de London
- Centre de Santé Communautaire Hamilton/Niagara
- Centre des services communautaires Vanier (CSCV)
- Centre Francophone de Toronto (CFT)
- Collège Boréal
- Conseil des Organismes Francophones de la Region Durham
- Conseil Economique et Social d'Ottawa-Carleton (CESOC)
- Contact Interculturel francophone de Sudbury
- FrancoQueer
- La Passerelle-Intégration et Développment
- La Société Économique de l’Ontario (SÉO)
- Le Centre ontarien de prévention des agressions (COPA)
- L'Institut de leadership des femmes de l'Ontario
- Maison d'Hébergement pour Femmes Francophones
- Oasis Centre des Femmes
- SOFIFRAN (Solidarité des femmes et familles immigrantes francophones du Niagara)


### Caucus des femmes
Ce Caucus conseille OCASI à propos des priorités des organisations de femmes. Il est formé de 38 organismes de femmes faisant partie d'OCASI et il est co-présidé par la représentante des femmes au Conseil d'administration. La première femme à occuper un tel poste a été élue par les membres d'OCASI lors de l'Assemblée générale annuelle de 2019.

Le Caucus a plaidé auprès du gouvernement provincial en vue d'un soutien aux programmes et aux services pour les femmes et il a réussi à obtenir un prolongement du financement pour « Investir dans l'avenir des femmes ». Le Caucus continue de plaider pour une augmentation du financement et pour un modèle de financement pluriannuel. Il a développé des priorités pour les élections fédérales et a organisé des formations en matière de médias et de sensibilisation publique afin que les membres puissent rehausser leurs capacités pour le plaidoyer.

Dans le cadre de la pandémie, les membres du Caucus sont en train de plaider activement en soutien aux femmes réfugiées, immigrantes et ayant un statut migratoire précaire.


#### Membres du caucus des femmes d'OCASI

**Central East / Centre-Est**

- Women's Multicultural Resource and Counselling Centre of Durham
- Women's Support Network of York Region

**Central West / Centre-ouest**

- Interim Place
- Lady Ballers Camp
- Sexual Assault and Violence Intervention Services of Halton
- The Women's Centre of Halton


**East / Est**

- Immigrant Women Services Ottawa


**North / Nord**  

- Northwestern Ontario Women's Centre

**South / Sud**

- Focus For Ethnic Women
- SOFIFRAN (Solidarité des femmes et familles immigrantes francophones du Niagara)

**Toronto**

- Afghan Women's Organization
- AWIC Community and Social Services
- Barbra Schlifer Commemorative Clinic
- Elspeth Heyworth Centre for Women
- FCJ Refugee Centre
- KCWA Family and Social Services
- L'Institut de leadership des femmes de l'Ontario
- Le Centre ontarien de prévention des agressions
- Maison d'Hébergement pour Femmes Francophones
- Margaret's Housing and Community Support Services Inc.
- Nellie's
- Newcomer Women's Services Toronto
- North York Women's Centre
- North York Women's Shelter
- Oasis Centre des Femmes
- Rexdale Women's Centre
- Scarborough Women’s Centre
- Sistering - A Woman's Place
- South Asian Women's Centre
- South Asian Women's Rights Organization
- The Redwood Shelter
- Times Change Women's Employment Service
- Vietnamese Women's Association of Toronto
- Women's Health in Women's Hands Community Health Centre
- WoodGreen Red Door Family Shelter  
- Working Women Community Centre
- YWCA Canada
- YWCA Toronto


**West / Ouest**

- Windsor Women Working With Immigrant Women
- Women's Enterprise Skills Training of Windsor Inc.
