---
title: Bâtir Les Capacités
thumbnail: /images/portfolio/graphic-capacity.jpg
postImage: /images/portfolio/graphic-capacity-post.jpg
preview: Forum des Directrices et Directeurs généraux, Conférence de développement professionnel, Programme d’éducation et de formation professionnelle, Formation en ligne (LearnAtWork.CA, Fondations d’établissement) SecteurEtablissement.Org, Alliés dans l’intégration des réfugiés, Reseau-etab.org
service: Investir Dans L'efficacité
date: 2020-07-13T12:00:02.000+06:00
---
## Investir Dans L'efficacité

### Les conférences d'OCASI

OCASI tient une Conférence de perfectionnement professionnel tous les deux ans, destiné aux travailleurs de première ligne de tous ses organismes membres. La conférence offre aux participants l'occasion de rehausser leurs habiletés, de partager des connaissances avec leurs pairs et de développer des stratégies de service aux clients.

Tous les deux ans aussi, nous tenons le Forum des Directrices et Directeurs généraux (Forum des DG) qui constitue un espace de dialogue pour les chefs de file du secteur autour de politiques publiques et de programmes. L'événement est destiné aux directeurs généraux et gestionnaires principaux des organismes membres d'OCASI et d'autres organisations du secteur, ainsi qu'à des représentants d'institutions publiques et d'autres groupes impliqués à l'établissement des immigrants et des réfugiés.

La Conférence de perfectionnement professionnel de 2019 a rassemblé 285 participants pendant trois jours de séances plénières, ateliers, discussions en caucus et l'Assemblée générale annuelle d'OCASI. La conférence a comporté une allocution d'ouverture par Stacey LaForme, chef élu de la Première Nation des Mississaugas de New Credit ainsi que des présentations principales effectuées par Angela Robertson, du Central Toronto Community Health Centre et Kimahli Powel, de Rainbow Railroad.

Le Forum des DG et l'Assemblée générale annuelle d'OCASI 2020 seront effectués de manière virtuelle.


285 participants au colloque
27 ateliers
3 séances axés sur les réalités francophones

« Toujours un forum fort enrichissant avec des informations et des mises en contexte utiles. »

« Encore une excellente conférence : bravo OCASI ! »

https://ocasi.org/professional-development-conference

https://ocasi.org/call-proposals-2020-ed-forum


### Éducation professionnelle et formation

Cette initiative a été lancée par OCASI en 2000 grâce au soutien financier du gouvernement fédéral afin de rehausser les habiletés et les connaissances des travailleurs d'établissement et ainsi renforcer les services offerts aux clients. OCASI offre une assistance financière à des travailleurs d'établissement individuels afin qu'ils puissent suivre des cours à l'institution de leur choix, et à des organismes afin qu'ils puissent offrir des formations sur mesure sur place à des groupes de travailleurs. L'initiative Éducation professionnelle et formation (PET) a été gérée et livrée avec succès pendant vingt ans, gagnant en popularité parmi les travailleurs et les organisations du secteur.

En 2019, 802 participants ont reçu une formation en groupe et 73 ont complété de cours de manière individuelle.

+ 800 participants à des formations en groupe
+ 70 apprenants individuels

« (...) La force principale de l'atelier, c'est qu'il a été offert de façon échelonnée, ce qui a permis d'établir une routine saine parmi le personnel et de compter sur des bienfaits qu'une séance unique n'aurait jamais pu offrir. Les membres du personnel ont pu choisir parmi plusieurs options, engageant ainsi l'activité qui répondait le mieux à leurs circonstances. »

« Je serai plus attentif aux détails dans les Plans d'établissement pour mes clients. Si vous n'êtes pas familiarisé avec le secteur de l'établissement, alors cette information est très utile. »

« Ces thèmes abordés en profondeur me permettent désormais de conduire des réunions efficaces avec mon équipe et de mieux performer lors des présentations auprès des clients, de mes collègues et des décideurs. J'ai également reçu des outils pratiques me permettant de soutenir mon équipe dans le cadre des services livrés aux nouveaux arrivants. »

« Le cours m'a offert des outils supplémentaires d'une énorme valeur pour les Conseillers en emploi qui aident les clients à utiliser des techniques actuelles de recherche d'emploi et à trouver leur voie efficacement dans le marché du travail. »

https://ocasi.org/professional-education-and-training-pet


### Settlement At Work/Secteur Établissement

Carrefour de connaissances pour le secteur des services aux immigrants et aux réfugiés en Ontario, ce site web offre des ressources en anglais et en français. Il aide les praticiens et les organismes du secteur à demeurer à jour concernant l'environnement général et les opportunités concrètes liées à leur travail. Le site offre des ressources tels des rapports de recherche, des manuels, des guides de meilleures pratiques et des formations en ligne. Les organismes du secteur peuvent y afficher leurs événements, offres d’emplois et appels de propositions.

Secteur Établissement a connu une augmentation de sa reconnaissance et de sa popularité. Parmi les utilisateurs, 89 % d’entre eux étaient nouveaux, une indication d'une audience francophone engagée et reconnaissante.


settlementatwork.org

+ 91 000 visiteurs uniques
+ 789 offres d’emplois
+ 200 nouvelles
+ 135 événements

secteuretablissement.org

+ 3 333 utilisateurs
+ 89 % nouveaux utilisateurs
+ 11 % d'augmentation du nombre d'utilisateurs
+15 % d'augmentation des séances
+ 150 000 impressions dans les médias sociaux

https://settlementatwork.org/

http://www.secteuretablissement.org/


### OrgWise

OCASI a lancé l'Initiative pour les Normes organisationnelles en 2007 afin de développer des normes volontaires par et pour le secteur des services aux immigrants et aux réfugiés. L'initiative comprend un outil d'auto-évaluation que les organismes peuvent utiliser pour évaluer leur propre santé et identifier des domaines de croissance potentielle. Elle offre également des ressources en ligne qui soutiennent le rehaussement des capacités organisationnelles.

Cette année, nous avons révisé et mis à jour le Guide de l'utilisateur et l'outil d'évaluation pour les rendre plus pertinents et plus aptes à aider les organisations à devenir plus efficaces. Nous avons mis à jour le site web afin d'améliorer l'expérience des utilisateurs, y compris en matière d'accessibilité et de multilinguisme, et nous avons ajouté de nouvelles fonctionnalités. OrgWise constitue une ressource à la base d'autres initiatives d'OCASI dont Maximizing Governance (Maximiser la gouvernance).

+ 3 500 visiteurs
+ 3 400 nouveaux visiteurs
+ 36 ressources affichées

 http://orgwise.ca/


### Maximiser la gouvernance au sein des organismes dédiés au bien public

Cette initiative offre de la formation en matière de gouvernance aux membres des conseils d'administration et aux leaders principaux des organismes à but non lucratif émergents à Toronto. Elle a aidé des groupes qui offrent des services d'importance critique pour leur communauté, les soutenant dans leur voie vers une stabilité et durabilité sur le long terme.

L'initiative a offert de la formation générale en matière de rôles et de responsabilités financières et juridiques, ainsi qu'un coaching sur mesure. L'initiative a atteint un ample éventail de communautés partout à Toronto, dont les jeunes, les personnes âgées, les femmes, les groupes racialisés, les personnes LGBTQ2I+ et les nouveaux arrivants. Un éventail de ressources a été développé pour cette initiative, dont des webinaires, des boîtes à outils et des feuilles de travail pour les organisations.

158 organismes participants
333 participants individuels

10 ateliers
12 séances d'apprentissage pratique
8 webinaires
17 organisations coachées individuellement

https://ocasi.org/maximizing-governance


### SettleNet.org/Reseau-etab.org

Cette plateforme en ligne bilingue est une initiative à l'intention du secteur des services aux immigrants et aux réfugiés partout au Canada. Elle est développée et soutenue par les organismes de regroupement régionaux du secteur et par certaines des plus grandes organisations de services aux immigrants et aux réfugiés au pays. La plateforme héberge plusieurs communautés de pratique, où ceux qui travaillent dans le secteur ou dans un domaine connexe peuvent se contacter les uns les autres afin d'apprendre, de partager et de collaborer.

Cette année, nous avons continué de développer et de bâtir la communauté et la plateforme SettleNet.org, nous concentrant sur l'engagement communautaire, la promotion et la liaison, y compris auprès des organismes francophones. Cela a mené vers une augmentation des membres de la communauté et du partage de contenus. Nous avons apporté des améliorations au design et à l'architecture de la plateforme afin d'offrir une expérience plus cohérente aux utilisateurs. L'an prochain, nous espérons voir une croissance durable de cette communauté de pratique en ligne.


+ 47 % d'augmentation des membres de cette communauté
+ 1 400 nouveaux membres
+ 1 600 nouveaux contenus publiés par la communauté
+ 90 groupes constitués
+ 127 100 lectures de pages
+ 20 900 séances

« Plateforme nationale bilingue robuste, SettleNet.org permet aux praticiens de l'établissement de traverser les frontières organisationnelles et géographiques pour tisser des liens et façonner un carrefour voué à l'apprentissage partagé. Nous sommes un secteur novateur et dynamique et nous avons besoin d'apprendre continuellement par l'entremise du partage de ressources, d'idées, de pratiques prometteuses, de défis, d'expertise et de connaissances. » – Nabiha Atallah, ISANS, Nouvelle-Écosse

« SettleNet.org est un carrefour qui permet de faire valoir les pratiques prometteuses issues de notre secteur. La communauté au sein de SettleNet.org repose sur notre compréhension et notre expertise quant à la nature et les défis des services d'établissement et d'intégration. Un des tout premiers documents sur les Meilleures pratiques d'établissement a été publié par le Conseil canadien pour les réfugiés en 1998. 21 ans plus tard, nous sommes fiers d'établir une plateforme en ligne pour les praticiens de partout au Canada. » – Sherman Chan, MOSAIC, Colombie-Britannique

« SettleNet.org permet aux praticiens de l'établissement de se connecter en ligne les uns avec les autres afin de partager des pratiques exemplaires et des connaissances. Une telle collaboration permet aux participants de travailler vers l'établissement de normes pour le secteur. Dans la mesure où les prestataires de services s'engagent dans un processus collectif de construction de connaissances et d'amélioration de la livraison des services, les nouveaux arrivants auront une transition plus aisée vers une vie productive et significative au Canada. » – Yasmin Dossal, COSTI, Ontario


### Alliés pour l'intégration des réfugiés

Menée par OCASI en étroite collaboration avec Refugee 613, l'initiative Alliés pour l'intégration des réfugiés engage des prestataires de services, des répondants privés, des réfugiés ayant été parrainés et d'autres parties intéressées en Ontario afin de trouver des manières d'améliorer la communication et de renforcer les liens afin de mieux soutenir la réinstallation des réfugiés. Notre recherche et notre travail avec les parties intéressées en 2019-2020 ont permis d'identifier plusieurs aspects à améliorer.

Dans le cadre de nos ateliers de conception collaborative tenus à Ottawa, Toronto et Kitchener-Waterloo, nous avons engagé 57 participants et développé plus de 15 idées de projets pilotes. Nous avons lancé un projet pilote autour de deux éléments (la gestion de cas et l'échange de connaissances) dans trois communautés en Ontario. En réponse au contexte de la pandémie, nous avons travaillé avec les partenaires du projet pilote et d'autres organismes du secteur pour tenir une table ronde virtuelle avec plus de 70 participants pour identifier des lacunes et des opportunités de soutien aux réfugiés parrainés.

Commentaires des participants aux séances de conception collaborative :

« Le projet proposé complètera nos programmes d'établissement centrés sur les clients en réponse aux divers besoins des nouveaux arrivants dans notre région. Notre but est de bâtir des relations plus fortes entre les travailleurs de l'établissement, les groupes de parrainage et les réfugiés parrainés récemment arrivés. » – YMCA de Kitchener-Waterloo

« Nous avons hâte de mettre au point un système de soutien à l'établissement mieux coordonné, sur mesure et durable pour le bien des futurs clients réfugiés parrainés. » – Catholic Crosscultural Services

« Ce projet pilote est fortement aligné sur le mandat d'ISKA de soutenir les nouveaux arrivants dans leur établissement et leur intégration au Canada. Les relations avec des partenaires communautaires, dont les répondants privés, sont essentielles à la prestation de services d'établissement intégrés. – Immigrant Services Kingston and Area

http://ocasi.org/allies-refugee-integration

Infographie présentant les résultats des séances de conception collaborative :
https://ocasi.org/sites/default/files/co-designing-success.pdf


### Formation en ligne

#### Les fondements du travail d’établissement

Nous avons lancé un nouveau cours en anglais et en français couvrant l’histoire, les lois et les politiques de l’immigration, les éléments de base du travail d’établissement, ainsi que les obstacles et les défis communs affectant les nouveaux arrivants au Canada. Le contenu de ce cours a également été emballé en format de livre.

+ 200 individus formés

« Je pense que ce cours introductoire est d’une grande valeur pour les travailleurs d’établissement de première ligne, mais AUSSI pour d’autres membres du personnel. »

« [Le cours] m’a vraiment amené à penser les choses autrement et à voir comment elles sont inter-reliées la plupart du temps. »


#### LearnAtWork.Ca

Lancé en 2008, le site web Learn At Work offre un éventail de cours en format animé ou auto-administré à l’intention des travailleurs servant les immigrants et les réfugiés, afin qu'ils puissent augmenter leurs habiletés de service aux nouveaux arrivants en Ontario.  Une moyenne de 28 cours sont offerts, abordant des sujets tels l’emploi, les réponses à la violence basée sur le genre, les services aux jeunes nouveaux arrivants, la santé mentale et l’accessibilité, entre autres. Deux nouveaux cours ont été ajoutés, sur la Solidarité entre les Autochtones et les immigrants et sur l'Approche centrée sur le traumatisme et la violence.

Notre plateforme en ligne a été mise à jour, l'adaptant aux téléphones mobiles. Un nouveau widget a été ajouté pour rendre le site plus accessible par le biais du contraste, de la navigation au clavier, d'une plus grande taille du texte, entre autres. D'autres améliorations ajoutées : des fichiers d'audio et des fonctionnalités interactives.

Dans le contexte de la pandémie, il y a eu une augmentation significative des inscriptions aux cours ainsi que des interactions avec les utilisateurs du site.

https://Learnatwork.ca

640 certificats délivrés
12 250 utilisateurs actifs
+ 1 million de visiteurs au site depuis 2008
+ 11 000 certificats délivrés depuis 2008

« J'aimerais prendre cette occasion pour VOUS REMERCIER pour tout l'excellent travail et le soutien que vous nous offrez en notre qualité de travailleurs aux premières lignes des services d'établissement. C'est une opportunité merveilleuse de rehausser nos connaissances et d'apprendre à propos d'un éventail de sujets pertinents pour notre travail auprès des familles et des jeunes nouveaux arrivants. »

« Nos remerciements vont à l'équipe et tout particulièrement à OCASI d'avoir offert cette formation à très haute valeur. J'ai acquis de nouvelles connaissances sur chaque sujet. »

« Comme toujours, vous avez été rapides et d'une aide remarquable. »

Image at the link below:
https://docs.google.com/document/d/1XB8SHXwh1MaWhfKSo8VJG9tFMJPUKChCshwsNAag_kM/edit
