---
title: Sud
featureImage: images/agencies/region-south.jpg
postImage: images/agencies/region-south.jpg
members: 16 membres
agencies: >
  - Beamsville

  - Brantford

  - Cambridge

  - Fort Erie

  - Guelph

  - Hamilton

  - Kitchener

  - Niagara

  - St. Catharines

  - Waterloo

  - Welland

---
- Centre de Santé Communautaire Hamilton/Niagara
- Employment Help Centre
- Focus For Ethnic Women
- Fort Erie Multicultural Centre
- Hamilton Centre for Civic Inclusion
- Hamilton Urban Core Community Health Centre
- Immigrant Culture and Art Association
- Immigrant Services Guelph-Wellington
- Immigrants Working Centre
- Kitchener-Waterloo Multicultural Centre
- Kitchener-Waterloo YMCA Immigrant Services
- Niagara Folk Arts Multicultural Centre
- Reception House - Waterloo Region
- SOFIFRAN (Solidarité des femmes et familles immigrantes francophones du Niagara)
- Welland Heritage Council and Multicultural Centre
- YMCA of Hamilton/Burlington/Brantford, Immigrant & Newcomer Services
