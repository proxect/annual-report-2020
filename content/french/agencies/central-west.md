---
title: Centre-Ouest
featureImage: images/agencies/region-central-west.jpg
postImage: images/agencies/region-central-west.jpg
members: 20 membres
agencies: >
  - Peel Region (Brampton)

  - Halton

  - Halton

  - Mississauga

  - Oakville

---
- African Community Services of Peel
- Al-Qazzaz Foundation for Education & Development
- Brampton Multicultural Community Centre  
- Centre for Education and Training
- Chinese Association of Mississauga
- Community Alliance for Support and Empowerment
- Dixie Bloor Neighbourhood Centre
- HMC Connections
- Interim Place
- Labour Community Services of Peel Inc.  
- Lady Ballers Camp
- Malton Neighbourhood Services
- Muslim Community Services
- Newcomer Centre of Peel
- Peel Career Assessment Services Inc.
- Peel Multicultural Council
- Punjabi Community Health Services  
- Roots Community Services Inc.
- Sexual Assault and Violence Intervention Services of Halton  
- The Women's Centre of Halton
