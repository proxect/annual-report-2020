---
title: Toronto
featureImage: images/agencies/region-toronto.jpg
postImage: images/agencies/region-toronto.jpg
members: 140 membres
agencies: >
  - Ville de Toronto

---
- Abrigo Centre  
- Access Alliance Multicultural Health and Community Services  
- Accessible Community Counselling and Employment Services   
- Afghan Association of Ontario   
- Afghan Women's Organization  
- Agincourt Community Services Association  
- Alliance for South Asian AIDS Prevention   
- Anglican United Refugee Alliance   
- Arab Community Centre of Toronto  
- Asian Community AIDS Services  
- Auberge Francophone  
- AWIC Community and Social Services  
- Bangladeshi-Canadian Community Services   
- Barbra Schlifer Commemorative Clinic  
- Black Coalition for AIDS Prevention  
- CAFCAN Social Services  
- Canadian Arab Federation  
- Canadian Centre for Language & Cultural Studies Inc.   
- Canadian Centre for Victims of Torture   
- Canadian Tibetan Association of Ontario  
- CARE Centre for Internationally Educated Nurses  
- Catholic Cross-Cultural Services  
- CATIE - Canadian AIDS Treatment Information Exchange  
- Centre for Immigrant and Community Services   
- Centre for Spanish Speaking Peoples   
- Centre Francophone de Toronto   
- Collège Boréal  
- Community Action Resource Centre  
- Community Family Services of Ontario 
- Community Legal Education Ontario   
- COSTI Immigrant Services  
- Council of Agencies Serving South Asians   
- CUIAS Immigrant Services (Canadian Ukrainian Immigrant Aid Society)  
- CultureLink  
- Davenport-Perth Neighbourhood and Community Health Centre  
- Delta Family Resource Centre  
- Dixon Hall  
- East Metro Youth Services  
- Eastview Neighbourhood Community Centre  
- Elspeth Heyworth Centre for Women  
- EnVision Education Foundation  
- Eritrean Canadian Community Centre of Toronto  
- Ethiopian Association in the Greater Toronto Area and Surrounding Regions  
- Family Inter-Generation Link  
- Family Service Toronto  
- FCJ Refugee Centre  
- Findhelp Information Services  
- For You Telecare Family Service   
- For Youth Initiative in Toronto  
- FrancoQueer  
- Harriet Tubman Community Organization   
- Heritage Skills Development Centre  
- Hong Fook Mental Health Association  
- IG Vital Health Inc.  
- Immigrant Women's Health Centre  
- Irish Canadian Immigration Centre  
- Jane Alliance Neighbourhood Services  
- Jane/Finch Community and Family Centre  
- Japanese Social Services, Toronto  
- Jewish Immigrant Aid Services, Toronto  
- JobStart  
- JVS Toronto  
- Kababayan Community Service Centre  
- KCWA Family and Social Services  
- L'Institut de leadership des femmes de l'Ontario  
- La Passerelle-Intégration et Développment  
- Lakeshore Area Multi Services Project  
- Lao Association of Ontario  
- Le Centre ontarien de prévention des agressions   
- Learning Disabilities Association of Toronto District  
- Madison Community Services  
- Maison d'Hébergement pour Femmes Francophones  
- Margaret's Housing and Community Support Services Inc.  
- Mennonite New Life Centre of Toronto  
- Multilingual Community Interpreter Services, Ontario  
- Neighbourhood Link Support Services  
- Nellie's  
- New Canadian Community Centre  
- New Circles Community Services  
- Newcomer Women's Services Toronto   
- North York Community House  
- North York Women's Centre  
- North York Women's Shelter  
- Northwood Neighbourhood Services  
- Oasis Centre des Femmes  
- Parkdale Community Information Centre  
- Parkdale Intercultural Association  
- Parkdale Queen West Community Health Centre 
- Polycultural Immigrant Community Services  
- Progress Career Planning Institute  
- Project Abraham 
- Rexdale Women's Centre  
- Roma Community Centre  
- S.E.A.S. (Support, Enhance, Access, Service) Centre  
- Salvadoran Canadian Association of Toronto   
- Scadding Court Community Centre  
- Scarborough Women’s Centre  
- Settlement Assistance and Family Support Services  
- Silent Voice Canada  
- Sistering - A Woman's Place  
- Skills for Change  
- Social Planning Toronto  
- Sojourn House  
- Somali Immigrant Aid Organization  
- South Asian Women's Centre  
- South Asian Women's Rights Organization  
- South Etobicoke Community Legal Services  
- St. Stephen's Community House  
- TAIBU Community Health Centre 
- Tesoc Multicultural Settlement Services  
- The 519  
- The Career Foundation  
- The Cross-Cultural Community Services Association  
- The Learning Enrichment Foundation  
- The Redwood Shelter  
- The Salvation Army, Immigrant and Refugee Services  
- The Teresa Group  
- Times Change Women's Employment Service  
- TNO - The Neighbourhood Organization  
- Toronto Centre for Community Learning & Development  
- Toronto Community & Culture Centre  
- Toronto Community Employment Services  
- Toronto Region Immigrant Employment Council   
- Toronto Ward Museum  
- Toronto Workforce Innovation Group  
- Tropicana Community Services  
- Turtle House Art/Play Centre  
- University Settlement  
- Vietnamese Association, Toronto  
- Vietnamese Women's Association of Toronto  
- West Neighbourhood House  
- Windmill Microlending 
- Women's Health in Women's Hands Community Health Centre  
- WoodGreen Community Services  
- WoodGreen Red Door Family Shelter  
- Workers' Action Centre  
- Working Skills Centre  
- Working Women Community Centre   
- World Education Services   
- YMCA of Greater Toronto, Newcomer Settlement Programs  
- YWCA Canada  
- YWCA Toronto  
