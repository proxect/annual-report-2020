---
title: Mission et Principes
topTitle: À propos d’OCASI
featureImage: images/about/about-debbie-manjeet-h.jpg
postImage: images/about/about-debbie-manjeet-h.jpg
date: 2020-07-13T12:00:01.000+06:00
service: À propos d’OCASI

---
## À propos d’OCASI

### Mission

La mission d’OCASI est d’atteindre l’égalité, l’accès et la participation pleine des immigrants et réfugiés à tous les aspects de la vie au Canada.


### Principes

OCASI affirme le droit de toutes les personnes de participer pleinement et équitablement à la vie sociale, culturelle, politique et économique de l’Ontario. OCASI affirme que les immigrants et les réfugiés au Canada devraient être garantis un accès équitable à tous services et programmes. OCASI croit que le Canada doit être une terre de refuge et d’opportunité, un pays connu pour l’humanité et la justice de son traitement des immigrants et des réfugiés.

OCASI croit que, en coopération avec d’autres groupes et communautés qui font la promotion des droits de l’homme et lutte contre la discrimination, OCASI verra ces principes réaliser. La mission d’OCASI est d’atteindre l’égalité, l’accès et la participation pleine des immigrants et réfugiés à tous les aspects de la vie au Canada.
