---
title: Message du DG
featureImage: images/about/debbie-douglas.jpg
postImage: images/about/debbie-douglas.jpg
date: 2020-07-13T13:06:26.000+06:00
date: 2020-07-13T12:00:03.000+06:00
service: Rehausser Notre Impact

---
## Rehausser Notre Impact

### Mot de La Présidente et de La Directrice Générale

D'habitude, notre message annuel constitue une réflexion sur l'année fiscale précédente : un regard sur nos accomplissements entre le 1er avril 2019 et le 31 mars 2020. Mais cette année est un peu différente.

À la mi-mars, l'Ontario a déclaré l'état d'urgence face à la COVID-19. OCASI a vite réagi depuis le début, appelant les gouvernements à maintenir les services du secteur et à accorder aux organismes la flexibilité nécessaire pour répondre de manière rapide à leurs clients et à leurs communautés. En vue de prendre le pouls du secteur, nous avons effectué des rencontres virtuelles qui ont suscité sur de très hauts niveaux de participation des organismes et des travailleurs de première ligne. Nous avons réalisé des webinaires en vue d'offrir de l'information opportune à propos des soutiens et des services aux organismes et nous avons tenu nos membres à jour par le biais de notre liste d'envois électroniques.

Le printemps 2020 a été marqué par la douleur et la colère collectives au sein des communautés noires du Canada et de l'Amérique du nord, ainsi que par l'appel à la justice. Cela a constitué également un moment clé pour notre secteur.  Nous devons agir et travailler pour un changement systémique progressiste, antiraciste et anticolonial.

L'année fiscale 2019-2020 a commencé sur une note d'espoir pour OCASI alors que nous élisions la première représentante qui occuperait le siège de Directrice des femmes au sein de notre Conseil d'administration. Le Caucus des femmes d'OCASI poursuit son travail de toute force grâce à l'engagement actif des organismes de femmes et des femmes racialisées faisant preuve de leadership.

OCASI étant le plus grand conseil sectoriel régional de notre secteur, nous avons plaidé d'une voix forte pour des politiques progressistes en matière d'immigration et d'asile, pour un investissement solide et durable dans les domaines de l'établissement et de l'intégration, et pour une approche explicitement antiraciste et contre les oppressions dans les stratégies fédérales et provinciales concernant l'équité de genre, la pauvreté et le logement. Nous avons promu la mise au point d'un plan d'action national contre le racisme pour répondre au racisme envers les Noirs et envers les Autochtones, ainsi que d'un programme de régularisation pour les personnes sans statut migratoire.

La force et la créativité de nos organismes membres nous inspirent à bâtir des communautés accueillantes pour tous les réfugiés, immigrants et migrants. Leur engagement et leur résilience sont pour nous une grande source d'énergie.


– **Manjeet Dhiman**, *Présidente*  

– **Debbie Douglas**, *Directrice générale*
