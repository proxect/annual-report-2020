---
title: Conseil d’administration
topTitle: Directeurs D’ocasi
featureImage: images/about/collage-board.jpg
postImage: images/about/collage-board.jpg
date: 2020-07-13T12:00:04.000+06:00
service: Directeurs D’ocasi

---
## Directeurs D’ocasi

### Comité Exécutif

#### Présidente
**Manjeet Dhiman** – ACCES Employment

#### Vice-présidente
**Moy Wong-Tam** – Centre for Immigrant and Community Services

#### Vice-présidente
**Valerian Marochko** – London Cross Cultural Learner Centre

#### Secrétaire
**Tracy Callaghan** – Adult Language and Learning

#### Trésorière
**Janet Madume** – Welland Heritage Council & Multicultural Centre / Employment Solutions

#### Ancien président
**Ibrahim Absiye** – CultureLink

### Directeurs Régionaux
#### Centre-Est
**Pam DeWilde** – Welcome Centre Immigrant Services – Pickering

#### Centre-Ouest
**Abi Ajibolade** – Lady Ballers Camp

#### Est
**Mercy Lawluvi** – Immigrant Women Services Ottawa

#### Nord
**Karol Rains** – Sault Community Career Centre

#### Sud
**Inés Rios** – Immigrants Working Centre

#### Toronto
**Manjeet Dhiman** – ACCES Employment

**Ahmed Hussein** – TNO - The Neighbourhood Organization

#### Ouest
**Valerian Marochko** – London Cross Cultural Learner Centre

### Directeurs Provinciaux
**Tracy Callaghan** – Adult Language and Learning

**Jeff Kariuki** – Job Skills - Markham North Welcome Centre

**Emily Kovacs** – Niagara Folk Arts Multicultural Centre

**Janet Madume** – Welland Heritage Council & Multicultural Centre / Employment Solutions

**Deepa Mattoo** – Barbra Schlifer Commemorative Clinic

**Randa Meshki** – Le Centre Ontarien de Prevention des Agressions

**Moy Wong-Tam** – Centre for Immigrant and Community Services

**Paulina Wyrzykowski** – St. Stephen’s Community House

### Directeur Francophone
**Saint-Phard Désir** – Conseil Economique et Social d’Ottawa-Carleton

### Directrice des femmes
**Fatima Filippi**

### Comité Permanent
- Exécutif
- Des Finances
- Francophone
- Gouvernance
- Services Aux Membres
- Politiques et Recherches
- Caucus Des Femmes
