---
title: OCASI Founding Members
featureImage: images/about/image-founders.jpg
postImage: images/about/image-founders.jpg
date: 2020-07-13T12:00:02.000+06:00
service: Visionaries

---
## Visionaries
### Portrait

Bloor Information and Life Skills Centre

Centre for Spanish Speaking Peoples

Chinese Community Services

Cross Cultural Communication Centre

Eastview Community Centre

Jewish Immigrant Aid Services

Jewish Vocational Services

Kababayan Community Centre

Parkdale Intercultural Association

Polish Immigrant & Community Services

St. Stephen’s Community House

Sudbury Multicultural Centre

University Settlement House

Vietnamese Association

WoodGreen Community Centre

Working Women Community Centre

YWCA of Metro Toronto (Immigrant Women Services)
