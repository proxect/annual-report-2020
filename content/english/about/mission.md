---
title: Mission and Principles
topTitle: About OCASI
featureImage: images/about/about-debbie-manjeet-h.jpg
postImage: images/about/about-debbie-manjeet-h.jpg
date: 2020-07-13T12:00:01.000+06:00
service: About OCASI

---
## About OCASI

### Mission

The Mission of OCASI is to achieve equality, access and full participation for immigrants and refugees in every aspect of Canadian life.


### Principles

OCASI asserts the right of all persons to participate fully and equitably in the social, cultural, political and economic life of Ontario.

OCASI affirms that immigrants and refugees to Canada should be guaranteed equitable access to all services and programs.

OCASI believes that Canada must be a land of refuge and opportunity, a country known for humanity and justice in its treatment of immigrants and refugees.

OCASI believes that in cooperation with other groups and communities which promote human rights and struggle against discrimination, OCASI will see these principles realized.
