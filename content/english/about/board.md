---
title: Board of Directors
topTitle: Dedicated Leadership
featureImage: images/about/collage-board.jpg
postImage: images/about/collage-board.jpg
date: 2020-07-13T12:00:04.000+06:00
service: Dedicated Leadership

---
## Dedicated Leadership

### Executive Committee

#### President
**Manjeet Dhiman** – ACCES Employment

#### Vice-President
**Moy Wong-Tam** – Centre for Immigrant and Community Services

#### Vice-President
**Valerian Marochko** – London Cross Cultural Learner Centre

#### Corporate Secretary
**Tracy Callaghan** – Adult Language and Learning

#### Treasurer
**Janet Madume** – Welland Heritage Council & Multicultural Centre / Employment Solutions

#### Past President
**Ibrahim Absiye** – CultureLink

### Regional Directors
#### Central East
**Pam DeWilde** – Durham Region Unemployed Help Centre

#### Central West
**Abi Ajibolade** – Lady Ballers Camp

#### East
**Mercy Lawluvi** – Immigrant Women Services Ottawa

#### North
**Karol Rains** – Sault Community Career Centre

#### South
**Inés Rios** – Immigrants Working Centre

#### Toronto
**Manjeet Dhiman** – ACCES Employment

**Ahmed Hussein** – TNO - The Neighbourhood Organization

#### West
**Valerian Marochko** – London Cross Cultural Learner Centre

### Provincial Directors
**Tracy Callaghan** – Adult Language and Learning

**Jeff Kariuki** – Job Skills - Markham North Welcome Centre

**Emily Kovacs** – Niagara Folk Arts Multicultural Centre

**Janet Madume** – Welland Heritage Council & Multicultural Centre / Employment Solutions

**Deepa Mattoo** – Barbra Schlifer Commemorative Clinic

**Randa Meshki** – Le Centre Ontarien de Prevention des Agressions

**Moy Wong-Tam** – Centre for Immigrant and Community Services

**Paulina Wyrzykowski** – St. Stephen’s Community House

### Francophone Director
**Saint-Phard Désir** – Conseil Economique et Social d’Ottawa-Carleton

### Women's Director
**Fatima Filippi**

### Board Standing Committees
- Executive
- Finance
- Francophone
- Governance
- Membership Services
- Policy and Research
- Women’s Caucus
