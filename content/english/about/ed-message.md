---
title: ED Message
featureImage: images/about/debbie-douglas.jpg
postImage: images/about/debbie-douglas.jpg
date: 2020-07-13T13:06:26.000+06:00
date: 2020-07-13T12:00:03.000+06:00
service: Building Impact

---
## Building Impact

### A Message from the President and the Executive Director

Our annual message is typically a reflection on the past fiscal year – a look back on our accomplishments from April 1, 2019 to March 31, 2020. But this year is a little different.

Ontario declared a state of emergency because of COVID-19 in mid-March. OCASI moved quickly from the start to call on governments to sustain sector services and allow agencies the flexibility to respond swiftly to their clients and communities. We held packed virtual town hall meetings for agencies and frontline workers to take the pulse of the sector. We arranged webinars to provide timely information on organizational supports and services, and kept our member agencies updated via our email list.

The Spring of 2020 was filled with deep collective pain and rage for Canada and North America’s Black communities and a call for justice. It was a key moment for our sector as well. We must act to work for progressive, anti-racist, anti-colonial systemic change.

The 2019-2020 fiscal year began on a hopeful note for the Council having elected the first Women’s Director seat on the OCASI Board. The OCASI Women’s Caucus continues in strength with active engagement by women’s organizations and racialized women leaders.

As the largest regional sector Council we were a strong voice in advocating for a progressive immigration and refugee policy, strong and sustained investment in settlement and integration, and an explicit anti-racism and anti-oppression lens in federal and provincial strategies regarding gender equity, poverty and housing. We called for a national anti-racism action plan to address anti-Black racism and anti-Indigenous racism, and a regularization program for people without immigration status.

Our member agencies inspire us with their strength and creativity in building communities that welcome all refugees, immigrants and migrants. We are energized by their commitment and resilience.


– **Manjeet Dhiman**, *President*  

– **Debbie Douglas**, *Executive Director*
