---
title: East Region
featureImage: images/agencies/region-east.jpg
postImage: images/agencies/region-east.jpg
members: 22 members
agencies: >
  - Belleville

  - Kingston

  - Ottawa

---
- Actions Interculturelles Canada
- Association Canadienne-Francaise de L'Ontario Conseil Regional Des Milles-Illes
- Catholic Centre for Immigrants - Ottawa
- Centre des services communautaires Vanier
- Conseil Economique et Social d'Ottawa-Carleton  
- Eastern Ontario Training Board
- English Language Training for the Ottawa Community  
- Immigrant Women Services Ottawa
- Iraqi Outreach Center
- Jewish Family Services of Ottawa-Carleton
- KEYS Job Centre
- Kingston Community Health Centres
- La Société Économique de l’Ontario  
- Lebanese and Arab Social Services Agency of Ottawa-Carleton
- National Capital Region YMCA-YWCA - Newcomer Information Centre  
- Ottawa Chinese Community Service Centre
- Ottawa Community Immigrant Services Organization  
- Ottawa Community Loan Fund  
- Quinte United Immigrant Services
- Somali Centre for Family Services
- Vitesse Re-Skilling Canada Inc.
- World Skills Employment Centre
