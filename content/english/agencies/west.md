---
title: West Region
featureImage: images/agencies/region-west.jpg
postImage: images/agencies/region-west.jpg
members: 20 members
agencies: >
  - Chatham

  - Leamington

  - London

  - Sarnia

  - Windsor-Essex

---
- Across Languages Translation and Interpretation  
- Adult Language and Learning  
- Centre communautaire francophone Windsor-Essex-Kent 
- Centre communautaire régional de London  
- London Cross Cultural Learner Centre  
- London Employment Help Centre  
- LUSO Community Services  
- Multicultural Council of Windsor and Essex County  
- New Canadians' Centre of Excellence Inc.   
- Nigerian-Canadians for Cultural, Educational, and Economic Progress  
- Northwest London Resource Centre 
- Ready-Set-Go Birth to Six Parental Support Group of Windsor  
- South Essex Community Council  
- South London Neighbourhood Resource Centre  
- Unemployed Help Centre of Windsor  
- WIL Employment Connections  
- Windsor Women Working With Immigrant Women  
- Women's Enterprise Skills Training of Windsor Inc.  
- YMCA of Western Ontario  
- YMCAs across Southwestern Ontario 
