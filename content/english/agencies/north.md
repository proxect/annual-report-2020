---
title: North Region
featureImage: images/agencies/region-north.jpg
postImage: images/agencies/region-north.jpg
members: 12 members
agencies: >
  - Kenora

  - North Bay

  - Sault Ste. Marie

  - Sudbury

  - Thunder Bay

---
- Contact Interculturel francophone de Sudbury
- D.O.O.R.S. to New Life Refugee Centre Inc.
- Multicultural Association of Kenora and District
- Multicultural Association of North Western Ontario
- North Bay & District Multicultural Centre
- Professions North / Nord
- Sault Community Information & Career Centre Inc.
- Sudbury Multicultural Folk Arts Association
- Thunder Bay Multicultural Association
- Yes Employment Services Inc. (Nipissing)
- YMCA of Northeastern Ontario, Sudbury
