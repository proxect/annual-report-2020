---
title: Central East Region
featureImage: images/agencies/region-central-east.jpg
postImage: images/agencies/region-central-east.jpg
members: 12 members
agencies: >
  - Barrie

  - Bradford

  - Durham Region (Ajax, Oshawa, Pickering, Whitby)

  - Peterborough

  - York Region (Aurora, Markham, Newmarket, Richmond Hill, Vaughan)

---
- Bradford Immigrant and Community Services
- Canadian Mental Health Association Durham
- Catholic Community Services of York Region  
- Community Development Council Durham  
- Conseil des Organismes Francophones de la Region Durham
- Durham Region Unemployed Help Centre
- Job Skills
- New Canadians Centre Peterborough
- Social Enterprise for Canada
- Women's Multicultural Resource and Counselling Centre of Durham
- Women's Support Network of York Region
- YMCA of Simcoe/Muskoka, Newcomer Services Department
