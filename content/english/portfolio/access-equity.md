---
title: Access and Equity
thumbnail: /images/portfolio/graphic-access-v.jpg
postImage: /images/portfolio/graphic-access-post.jpg
preview: Positive Spaces Initiative, Accessibility Initiative, Mental Health Promotion, Immigrant Refugee Communities, Neighbours, Families and Friends Campaign, Building Leadership Capacity - GBV
service: A more equitable sector
date: 2020-07-13T12:00:04.000+06:00
---
## A more equitable sector

### Positive Spaces Initiative

Positive Spaces Initiative supports the immigrant and refugee-serving sector to more effectively serve LGBTQIA+ (lesbian, gay, bisexual, trans, two-spirit, queer, questioning, intersex, asexual, pansexual, genderqueer, etc.) newcomers. We continued to provide training to sector agencies to build capacity, and support them to become designated positive spaces. Thanks to our regional champions, we saw increased activity in our regional networks, with over 9 events hosted across Ontario, including a Legal Aid Clinic Launch in Cambridge, a play in St. Catharines featuring LGBTQIA+ newcomer stories, and a picnic gathering in London for LGBTQIA+ newcomers. In response to strong demand we created 60 interactive resources to support our learning tools.

Together with OCASI’s Accessibility Initiative we held a joint bilingual roundtable discussion on addressing service barriers arising from bias and stigma within the sector. We also collaborated on recruiting Access and Equity allies to champion both initiatives and build stronger networks to make our spaces more positive, safe, accessible and inclusive for LGBTQIA+ newcomers and/or those with in/visible disabilities.

http://positivespaces.ca/


Quotes and stats – use where relevant

60 new interactive resources created

32 training workshops

+ 600 participants trained

+ 900 participants trained in online courses


“The videos were very helpful in bringing a different perspective. It "humanizes" everything we are learning and I think for folks who might have a harder time with these topics, might have a bigger impact. Or so I hope.”


« Les sujets abordés dans cette formation étaient très intéressant dû au fait que nous travaillons avec des adolescents qui, potentiellement pourraient vivre ces situations et je voudrais que tous mes collègues/employés puissent être en mesure de venir en aide à une personne qui vit quelque chose de semblable. »

“…I appreciated the opportunity to discuss issues arising where human rights seemingly conflict, as this is a topic in my workplace that sometimes is avoided or considered 'impossible'. I also appreciate the tools and opportunity to evaluate my own workplace and identify action plans to improve service…”

### Accessibility Initiative

This bilingual initiative supports immigrant and refugee service workers to acquire new skills and knowledge to provide services for immigrants and refugees with in/visible disabilities. Training and resources provided through the initiative support workers to gain a greater understanding of laws relating to the rights of people with disabilities, and the relationship between disability, race, immigrant/refugee status and other layers of marginalization. We held roundtable discussions for sector agencies and provided training in a variety of formats including in-person, online and self-directed e-learning.

We collaborated with OCASI’s Positive Spaces Initiative on a joint roundtable on bias and stigma. We also collaborated on recruiting and training allies to champion both initiatives.

https://ocasi.org/accessibility-program

Accessibility toolkit:
https://ocasi.org/sites/default/files/accessibility-kit_0.pdf


Quotes and stats – use where relevant

+ 1100 website visitors

2 new bilingual resources created

23 bilingual training workshops

+ 320 participants

14 bilingual webinars

+ 320 participants

+ 200 online learners



“The training was very useful as I was not so familiar with the laws regarding disabilities in Ontario, what we can do to help newcomers with disabilities.”

« Modèle interactif. Informations partagées étaient reliées à mon poste (travailleuse d’établissement) et je pourrais les appliquer dans mon travail de tous les jours. »

« Ça m’as permis de me mettre en situation et dans la peau d’une personne ayant un handicap les difficultés auxquelles ces personnes font face. »

« Très intéressant j’aimerais revoir l’atelier avec plus en profondeur. Merci à OCASI pour les formations en Français. »

### Mental Health Promotion

This initiative builds the capacity of immigrant and refugee serving organizations to promote mental health and address related issues among newcomer populations through collaborations and partnerships. It is based on a partnership between the immigrant and refugee serving sector and primary care and mental health sectors.

We collaborated with partners to develop an integrated service model, based on an environmental scan and needs assessment completed last year. We worked with Rexdale Women’s Centre and Polycultural Immigrant and Community Services to pilot the model. We hosted a well-attended two day knowledge exchange event with primary care, mental health, and immigrant and refugee-serving agencies. We continued to provide capacity building training to immigrant and refugee service workers on topics such as mental health first aid and reducing mental health stigma in the sector.

Quotes and stats – use where relevant


“[The training] … opened my eyes and understanding of how important it is to take care of myself and recognize when to ask for help/support”

“The training … put into perspective the importance of being mentally well in the workplace and not forgetting about one’s well-being above all.”


https://ocasi.org/mental-health-promotion

- TVIA Guidelines (English)
https://ocasi.org/sites/default/files/tvia-guide-english-online.pdf

TVIA Guidelines (French)
 https://ocasi.org/sites/default/files/tvia-guide-french-v3-2020.pdf


Quotes and stats – use where relevant


+ 175 workshop participants

“Thank you for providing such a wonderful training which is very useful for me as a frontline staff as the need is so high for individuals facing mental health concerns.”

“I will be able to share this with the youth that I work with and be a safe person they can trust.”

“I appreciated the focus on promoting staff mental health and leadership mental health in the workplace.”

### Immigrant Refugee Communities - Neighbours, Friends and Families Campaign

This initiative builds community capacity for bystander intervention in situations of domestic violence in immigrant and refugee communities. We trained and mentored 14 Peer Champions. The model was recognized as a promising practice by Pathways to Prosperity, the national alliance of university, community and government partners.

We digitized “What Would You Choose? - Scenario Cards”, and distributed these and other educational materials to immigrant and refugee-serving agencies, and a variety of other networks that engage with newcomers. The new digital cards were used by Peer Champions to educate and inform at community events, creating significant impact especially in communities with limited opportunity to hold safe discussions about domestic and gender-based violence. The digitalization of our tools nearly doubled our social media presence.

https://www.immigrantandrefugeenff.ca/


Quotes and stats – use where relevant

14 Peer Champions trained


+ 300 organizations engaged with the campaign


28 training workshops

1100 training participants

+ 4500 visitors to Campaign website

+ 74,000 social media impressions

“I felt very happy and satisfied while I fulfilled my duty of connecting the community to the much needed resources around them. I look forward to Peer Champion in the coming years, so that I can maintain and strengthen the bond which I developed with the community during the events.”

“This past year brought me more opportunities to connect with more people in the VAW sector and community members in my capacity as a peer champion. The peer role gave me more leverage to work with partners in the community in the fight against domestic violence. I continued my learning from survivors and community members through the weekly cooking program.”

### Building Leadership Capacity to Address Gender-Based Violence against Non-Status, Refugee and Immigrant Women across Canada

In this national initiative OCASI has partnered with non-profit organizations from across Canada to build leadership capacity of women with precarious immigration status to address gender-based violence. Partners include New Brunswick Multicultural Council, Immigrant Women Services of Ottawa, Barbra Schlifer Commemorative Clinic, Rights of Non-Status Women Network, Sexual Assault Center of Edmonton, Islamic Family Social Services Association and MOSAIC.
We completed an extensive environmental scan and needs assessment of existing supports and services established in community based-advocacy and survivor-led models, using a literature review, stakeholder consultations, survey, focus groups and interviews. We collaborated with our partners to develop a "Blueprint in Progress" to outline promising practices with peer champions and community advocacy networks. To support service providers we organized webinars and online training courses.
We look forward to launching a virtual roundtable series in the new fiscal year to bring together community advocates, sector leaders and workers and women with precarious immigration status to strategize around addressing gender-based violence. With the onset of the pandemic the roundtables will provide an opportunity to find ways to mitigate the impact of COVID-19 on this population.
https://ocasi.org/gender-based-violence

Quotes and stats – use where relevant

+ 270 needs assessment surveys completed

+ 70 stakeholders consulted
+ 100 training participants

"Thank you for an excellent presentation and sharing the great work that is being carried out across Canada!" - Roundtable Participant

"Thanks for the interesting and creative session. The artwork was fantastic and the participant information on leadership definitions was what I appreciated most. It is so great to work together." - Roundtable Participant

"Great discussion and inputs, working to gather, thank you for the opportunity and thanks for the presenters." - Roundtable Participant
