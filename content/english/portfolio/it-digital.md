---
title: IT and Digital Services
thumbnail: /images/portfolio/graphic-tech.jpg
postImage: /images/portfolio/graphic-tech-post.jpg
preview: Settlement.Org (Settlement.Org Discussion Forum), Etablissement.Org, NewYouth.ca, WelcomeOntario.ca OCASI Client Management System
service: It matters
date: 2020-07-13T12:00:03.000+06:00
---
## It matters

### Settlement.Org

This award-winning website is a trusted online resource for newcomers and immigrant and refugee-serving agencies in Ontario since 2000.

This year we added 18 articles and updated more than 53 to keep the site current and relevant. New content focused on immigration changes, health programs and employment benefits. Services Near Me, our interactive community services mapping tool in collaboration with Findhelp/211 continues to be a popular resource for users. We continue our many strong partnerships with organizations, institutions and government, such as Community Legal Education Ontario (CLEO), Windmill Microlending, and Ontario Ministry of Health, to provide timely updated content to newcomers.

Quotes & stats – use if/where relevant

“I appreciate your response and additional guidance on ESL Corner. I must say it's a great resource and very helpful for newcomer students.”

“I came to Canada when I was 17. My family used to look up a lot of information on the settlement.org website - it was a tremendous help to us.”


Users
+ 6.2 million

18 new articles
53 articles updated
384 events posted


Twitter followers
+ 7,400
+ 10,300 tweets

Facebook followers
+ 2,000

Facebook likes
+ 2000

[use the links below with social media logos?]


https://www.facebook.com/Settlement.Org

https://twitter.com/Settlement_Org

https://www.youtube.com/user/settlementorg

https://settlement.org/outreach/rss/

### Settlement.Org Discussion Forum

This peer-driven discussion forum features users asking questions about life in Ontario and sharing their experience with others. It continually directs and engages users with content on Settlement.Org.

Questions and concerns about Social Insurance cards and what one should do when they are lost or stolen was a major trend this year. Travelling to the United States as a permanent resident of Canada was a popular topic this year as it has been in previous years. Since March 2020, we observed a trend of new topics relating to the Canada Emergency Response Benefit (CERB), Employment Insurance and other financial benefits, as well as discussions on finding a job during the COVID pandemic.

Quotes & stats – use if/where relevant

Users
+ 37,500

Topics +3600

Posts +10,180

### Etablissement.Org

Etablissement.Org is a trusted information and referral website for francophone newcomers. The website covers topics ranging from immigration and employment to education and daily life.

This year we added over 20 new articles and updated more than 100. New and updated content focused mainly on health, personal finances, education and employment. The most popular content from the start of the pandemic were immigration, citizenship and Employment Insurance (EI). The site continues to grow in popularity. The total number of new users was over 728,000. In comparison, according to the 2016 Census Francophone immigrants were 92,385, making up 15% of the Francophone population of Ontario.

Quotes & stats – use if/where relevant

J’ai navigué un peu votre site, il y a une belle panoplie d'information

New users
+728,000

Page views
+1,199,400

Twitter followers
495

Links:

Êtes-vous nouveau en Ontario?
https://settlement.org/outreach/order-promotional-materials/view?image=new-to-ontario-poster&type=png

OCASI sur le Web
https://www.youtube.com/watch?v=3f-lRijCOew

[use the links below with social media logos?]

https://twitter.com/EtabSite

https://www.youtube.com/user/etablissementorg

https://etablissement.org/liaison/rss/

### NewYouth.ca / NouveauxJeunes.ca

Since 2008, these websites are the trusted resource for newcomer youth in Ontario for information on what it is like to study here, to work here, and the rights and services to which youth are entitled. The sites were redesigned last year and launched in August 2019. The upgrades made navigation easier for users, including a re-categorization of the articles and a language toggle between French and English.

This year we added articles on youth rights, education and immigration and other related topics. After March 2020, articles on renting and tenant rights, Employment Insurance (EI) and other financial assistance programs were the most popular among users, most likely driven by the COVID-19 pandemic.

Quotes & stats – use if/where relevant

Users
+ 96,510

Twitter followers
+ 2750

[use the links below with social media logos?]

https://twitter.com/newcomeryouth

### Welcome Ontario

WelcomeOntario / AccueilOntario provides information to support refugee resettlement by providing information and resources relevant to refugees and those who are involved in supporting their resettlement. The website helps to bridge the communications gap between private sponsors and settlement workers, and empowers sponsored refugees by giving them reliable information.

OCASI developed the websites in 2015, originally as a response to support Syrian refugee resettlement initiatives.

https://twitter.com/welcomeontario

https://www.facebook.com/WelcomeOntarioAccueilOntario/

### OCASI Client Management System

OCMS is a cloud-based data management system that helps to ensure that services to newcomers are delivered efficiently and effectively by providing tools that save both the worker and the client time. It is an OCASI social enterprise developed for the sector and by the sector, and supported only through user fees.

This year we added new features and functions to the system, such as new reports customizable for use by agencies, and an email feature to facilitate referrals between partner agencies. Future plans include providing improved online training options.

Quotes & stats – use if/where relevant

“Help Desk is always quick to respond and ultimately, usually solves my problem.
Excellent database!”

“Overall, OCMS has been a great experience for our organization and we appreciate the support and help!”

“I can now efficiently get my work completed, thank you.”

“Thank you for the hard work to switch over OCMS to a better network/servers/etc... it really made a difference that is hard to quantify.”

Total Users
2,220

Total Unique Clients
+ 1,018,000


Total sessions (from the beginning)
+ 2,486,000

Total reports generated per month
+ 4,000
