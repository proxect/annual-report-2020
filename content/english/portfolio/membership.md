---
title: Membership
thumbnail: /images/portfolio/graphic-membership-v.jpg
postImage: /images/portfolio/graphic-membership-post.jpg
preview: Francophone Committee, Women’s Caucus
service: Collective Impact
date: 2020-07-13T12:00:05.000+06:00
---
## Collective Impact
### Francophone Committee
The Committee advises OCASI on priorities for francophone agencies and francophone refugee and immigrant services. It is formed by the 20 francophone member agencies in the Council and is chaired by the Francophone Director of the Board.

The Committee successfully advocated to the federal government to establish a francophone kiosk at Toronto International Airport to assist newcomers. They hope to have the initiative reproduced in other communities, and increase the scope and scale of providing French language pre-arrival information. They were influential in having Ontario commit to 5% francophone immigration annually.

Committee members are collaborating actively during the pandemic to ensure francophone newcomers receive the support they need.

#### OCASI Francophone Committee Members

- Actions Interculturelles Canada
- Association Canadienne-Francaise de L'Ontario Conseil Regional Des Milles-Illes (ACFOMI)
- Auberge Francophone
- Centre communautaire francophone Windsor-Essex-Kent
- Centre communautaire régional de London
- Centre de Santé Communautaire Hamilton/Niagara
- Centre des services communautaires Vanier (CSCV)
- Centre Francophone de Toronto (CFT)
- Collège Boréal
- Conseil des Organismes Francophones de la Region Durham
- Conseil Economique et Social d'Ottawa-Carleton (CESOC)
- Contact Interculturel francophone de Sudbury
- FrancoQueer
- La Passerelle-Intégration et Développment
- La Société Économique de l’Ontario (SÉO)
- Le Centre ontarien de prévention des agressions (COPA)
- L'Institut de leadership des femmes de l'Ontario
- Maison d'Hébergement pour Femmes Francophones
- Oasis Centre des Femmes
- SOFIFRAN (Solidarité des femmes et familles immigrantes francophones du Niagara)

### Women’s Caucus
The Caucus advises OCASI on priorities for women’s organizations. It is formed by the 38 women’s organizations in the Council and is co-led by the Women’s Director. The first incumbent of the seat was elected by the OCASI membership at the 2019 Annual General Meeting.

The Caucus advocated to the provincial government for support for women’s programs and services and succeeded in having funding extended for “Investing in Women’s Future”. They continue to advocate for increase in funding and a shift to a multi-year funding model. They developed priorities for the federal election, and arranged media and public education training for members to build advocacy capacity.

Caucus members are actively advocating to support refugee, immigrant and precarious migrant women during the pandemic.

#### OCASI Women’s Caucus Members

**Central East Region**

- Women's Multicultural Resource and Counselling Centre of Durham
- Women's Support Network of York Region

**Central West Region**

- Interim Place
- Lady Ballers Camp
- Sexual Assault and Violence Intervention Services of Halton
- The Women's Centre of Halton

**East Region**

- Immigrant Women Services Ottawa

**North Region**  

- Northwestern Ontario Women's Centre

**South Region**

- Focus For Ethnic Women
- SOFIFRAN (Solidarité des femmes et familles immigrantes francophones du Niagara)

**Toronto Region**

- Afghan Women's Organization
- AWIC Community and Social Services
- Barbra Schlifer Commemorative Clinic
- Elspeth Heyworth Centre for Women
- FCJ Refugee Centre
- KCWA Family and Social Services
- L'Institut de leadership des femmes de l'Ontario
- Le Centre ontarien de prévention des agressions
- Maison d'Hébergement pour Femmes Francophones
- Margaret's Housing and Community Support Services Inc.
- Nellie's
- Newcomer Women's Services Toronto
- North York Women's Centre
- North York Women's Shelter
- Oasis Centre des Femmes
- Rexdale Women's Centre
- Scarborough Women’s Centre
- Sistering - A Woman's Place
- South Asian Women's Centre
- South Asian Women's Rights Organization
- The Redwood Shelter
- Times Change Women's Employment Service
- Vietnamese Women's Association of Toronto
- Women's Health in Women's Hands Community Health Centre
- WoodGreen Red Door Family Shelter  
- Working Women Community Centre
- YWCA Canada
- YWCA Toronto

**West Region**

- Windsor Women Working With Immigrant Women
- Women's Enterprise Skills Training of Windsor Inc.
