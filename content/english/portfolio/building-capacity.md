---
title: Building Capacity
thumbnail: /images/portfolio/graphic-capacity.jpg
postImage: /images/portfolio/graphic-capacity-post.jpg
preview: Executive Directors Forum, Professional Development Conference, Professional Education and Training, Online Training (LearnAtWork.CA, Foundations of Settlement), SettlementAtWork.Org, Allies in Refugee Integration, SettleNet.Org, Orgwise
service: Investing in Effectiveness
date: 2020-07-13T12:00:02.000+06:00
---
## Investing in Effectiveness

### OCASI Conferences

OCASI holds a Professional Development Conference for frontline workers from OCASI member agencies across Ontario every other year. The conference is intended for frontline workers to build capacity, network, and share knowledge with peers, and develop client service strategies.

We hold an Executive Directors’ Forum in alternating years as a policy and program dialogue space for sector leaders. The event is targeted to executive directors and senior managers from OCASI member agencies and other sector organizations, and from public institutions and other groups concerned with immigrant and refugee settlement.

The 2019 OCASI PD Conference brought together 285 participants for three days of plenaries, workshops, caucus discussions and the OCASI Annual General Meeting. The Conference featured an opening address by Stacey LaForme, elected Chief of the Mississaugas of the New Credit First Nation, and keynote presentations by Angela Robertson of Central Toronto Community Health Centre, and Kimahli Powell of Rainbow Railroad.

The 2020 OCASI ED Forum and the Annual General Meeting will be held virtually for the first time.

Quotes & stats – use if/where relevant

285 conference attendees
27 workshops
3 francophone-focused sessions

“Always a very enriching forum with useful information and context.”


“Another excellent conference - OCASI ROCKS.”

https://ocasi.org/professional-development-conference

https://ocasi.org/call-proposals-2020-ed-forum

### Professional Education and Training

This initiative was launched by OCASI in 2000 with funding support from the federal government to increase settlement workers’ skills and knowledge in order to strengthen client services. OCASI provides financial assistance to individual settlement workers to take courses at an institution of their choice, and to organizations to arrange customized in-house group training. The Professional Education and Training (PET) initiative has been successfully managed and delivered for over 20 years, and has grown in popularity among sector workers and organizations.

In 2019 a total of 802 participants received group training and 73 completed individual courses.

+800 group training participants

+ 70 individual learners

“…the main strength of the workshop was that it was ongoing and allowed for a healthy routine to be established amongst staff, with the benefits stretching beyond a one-off session. Staff had enough options to choose from in order to engage in the activity that best suited them.”

“I will be more detailed in my Settlement plans for my clients. If you are not new/ familiar with the Settlement sector then this information is very helpful”

 “Covering these topics in depth was useful to conduct effective meetings with my team, better perform in presentations in front of clients, colleagues and decision-makers. It also provided actionable tools to support my team when providing services to newcomers.”

“The course provided additional tools that are very valuable for Employment Advisors when coaching clients on present job search techniques and how to effectively navigate the labour market.”

https://ocasi.org/professional-education-and-training-pet

### Settlement At Work/Secteur Etablissement

The knowledge hub for the immigrant and refugee-serving sector in Ontario, this website provides resources in English and French. They help sector practitioners and organizations to remain updated on the general environment, as well as specific opportunities pertaining to their work. The website features resources such as research reports, manuals, best practice guides, and online training. Sector organizations can promote their events, jobs, and request for proposals.

Secteur Etablissement has been growing in recognition and popularity. 89% of total users were new, evidence of an engaged and appreciative francophone sector audience.

Quotes & stats – use if/where relevant

settlementatwork.org

+ 91,000 unique visitors

 Jobs posted
- 789

News items
+ 200

Events
- 135

secteuretablissement.org

+ 3,333 users

+ 89% new users

+ 11% increase in users

+ 15% increase in sessions

+150,000 social media impressions

https://settlementatwork.org/

http://www.secteuretablissement.org/

### OrgWise

OCASI initiated the Organizational Standards Project in 2007 to develop voluntary standards by and for the immigrant and refugee-serving sector. The initiative features a self-assessment tool for organizations to assess their own health and identify areas for growth, and provides online resources to support organizational capacity building.

This year we reviewed and updated the User Guide and the assessment tool to make them more relevant for organizations and support them in becoming more effective. We made updates to the website to improve the user experience, particularly in accessibility and multilingual capacity, and access to new website features. OrgWise is a central resource for other OCASI initiatives such as our Maximizing Governance project.

Quotes & stats – use if/where relevant

+ 3,500 total visitors

+ 3,400 new visitors

+ 36 resources posted

 http://orgwise.ca/

### Maximizing Governance in For Public Benefit Organizations

This initiative provides governance training to board members and senior leaders of small and emerging non-profit organizations in Toronto. It has helped groups that provide critically important services for their communities, and supports them to work toward long-term stability and sustainability.

The initiative provided general training on roles and on financial and legal responsibilities, as well as customized coaching. It reaches a wide variety of communities across Toronto including youth, seniors, women, racialized groups, LGBTQIA+, and newcomers. A variety of resources were developed for the initiative including webinars, toolkits, and worksheets for organizations.

Quotes & stats – use if/where relevant

158 participating organizations
333 individual participants

10 workshops
12 applied learning sessions
8 webinars
17 organizations coached individually


https://ocasi.org/maximizing-governance

### SettleNet.org/Reseau-etab.org

This bilingual online platform is a national initiative for the immigrant and refugee-serving sector across Canada. It is developed and supported by Canada’s regional sector umbrella organizations and some of the largest immigrant and refugee-serving agencies in the country. The platform facilitates a collection of communities of practice where those working in the sector and related field can connect with each other to learn, share, and collaborate.

We continue to develop and grow the SettleNet.org community and platform this year, with a focus on community engagement, promotion, and outreach, including to Francophone agencies. As a result we saw a significant increase in community members, and an increase in content sharing. We made improvements to the design and architecture of the platform to provide a more consistent user experience. Next year we look forward to sustaining and growing the online community of practice.

Quotes & stats – use if/where relevant

+ 47% increase in community members
+ 1400 new members
+ 1600 new content posted by the community
+ 90 groups started
+ 127,100 page views
+ 20,900 sessions

“SettleNet.org, a robust bilingual national platform, allows settlement practitioners to cross organizational and geographic boundaries to create connections and establish a practitioner-informed platform for shared learning. In this innovative and dynamic sector, we need to keep learning by sharing resources, ideas, promising practices, challenges, expertise and knowledge.”— Nabiha Atallah, ISANS, Nova Scotia

“SettleNet.org is a hub to demonstrate the promising practices that flow out of our sector. The community on SettleNet.org builds on our understanding and expertise of the nature and challenges of settlement and integration services. One of the very first documents on Best Settlement Practices was published by the Canadian Council for Refugees in 1998. 21 years later, we are proud to establish an on-line platform for practitioners across Canada.”— Sherman Chan, MOSAIC, British Columbia

"SettleNet.org, a robust bilingual national platform, allows settlement practitioners to connect online with the intention of sharing best practice and their learning. On SettleNet.org people are connecting and collaborating on setting sector standards. As a result of service providers engaging in the process of collective knowledge building and improved service delivery, newcomers will transition into productive and meaningful lives in Canada." — Yasmine Dossal, COSTI, Ontario

### Allies in Refugee Integration

Led by OCASI in close partnership with Refugee 613, Allies for Refugee Integration engages service providers, refugee sponsors, formerly sponsored refugees and other stakeholders in Ontario to find ways to improve communication and connection to support refugee resettlement. Our research and work with stakeholders in 2019-2020 highlighted several areas for improvement.

Through our collaborative design workshops in Ottawa, Toronto and Kitchener-Waterloo, we engaged 57 participants and developed more than 15 pilot ideas. We began a pilot test of two ideas – case management and knowledge exchange – in three Ontario communities. In response to the pandemic we worked with the pilot partners and other sector organizations to hold a virtual roundtable with more than 70 participants to identify gaps and opportunities in supporting sponsored refugees.

Quotes & stats – use if/where relevant


Comments from Collaborative Design Session participants:

“The proposed project will complement our existing client-centred settlement programs responding to the diverse newcomer needs in our area. It is our goal to build stronger relationships between settlement workers, sponsorship groups and sponsored refugee newcomers.” - YMCA of Kitchener-Waterloo

“We look forward to a more coordinated, tailored, and sustainable settlement support system for future privately sponsored refugee clients.” - Catholic Crosscultural Services

“This pilot is directly in line with ISKA’s mandate to support newcomers in their integration and settlement in Canada. Relationships with community partners, including Private Sponsors, are essential to providing integrated settlement services.” - Immigrant Services Kingston and Area

http://ocasi.org/allies-refugee-integration

Infographic of results of collaborative design sessions:
https://ocasi.org/sites/default/files/co-designing-success.pdf

### Online Training

#### Foundations of Settlement Work

We launched a new two-part online course in English and French that covers immigration history, laws and policy, the basics of settlement work, and common barriers and challenges for newcomers to Canada. The content was also produced in book form.

https://settlementatwork.org/resources/foundations-settlement-work-ontario


Quotes & stats – use if/where relevant


+ 200 trained

"I think this intro course is valuable to front line settlement workers AND other staff as well."

"[The course] really made me think of things in a different way and see how they are related in most cases.”

#### LearnAtWork.ca

Launched in 2008, the Learn at Work website provides a variety of free self-directed and facilitated online courses for immigrant and refugee service workers to enhance their skills in serving newcomers to Ontario. An average of 28 courses are offered on topics such as employment, addressing gender based violence, working with newcomer youth, mental health, accessibility and more, and two new courses on Indigenous Migrant Solidarity, and Trauma and Violence Informed Approach.

Our online platform was updated to make LearnAtWork.ca mobile friendly. A new widget was added to make the site more accessible through features such as contrast, keyboard navigation, larger text and more. Other improvements include adding audio files and interactive features to certain courses.

Course enrollment increased significantly as a result of the pandemic as did interactions with site users.

https://Learnatwork.ca

Quotes & stats – use if/where relevant
640 certificates issued (2019 – 2020)


12250 total active users


+ 1 million site visitors since 2008

+ 11,000 certificates issued since 2008


“I want to take this opportunity to "THANK YOU" for all the great work and support that you provide to us as front line workers in the settlement services. It is a wonderful opportunity to increase our knowledge and learn from a variety of topics relevant to our work with newcomer families and youth.”

“With due thanks to the team and especially to OCASI for having organized this valuable training I have gained more knowledge in every subject.”

“As always you are fast and helpful.”


Image at the link below:
https://docs.google.com/document/d/1XB8SHXwh1MaWhfKSo8VJG9tFMJPUKChCshwsNAag_kM/edit
