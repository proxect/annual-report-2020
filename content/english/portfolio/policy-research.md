---
title: Policy and Research
thumbnail: /images/portfolio/graphic-policy-v.jpg
postImage: /images/portfolio/graphic-policy-post.jpg
preview: Government relations, Sector relations, Research, Policy and Advocacy, Media relations
service: Advocacy for change
date: 2020-07-13T12:00:01.000+06:00
---
## Advocacy for change

### Policy

OCASI member agencies’ priorities and concerns are at the centre of our policy activities. Equitable access to services for refugees and all migrants, and fairness in immigration and refugee policies is a major focus.

We continue to call for a withdrawal from the Safe Third Country Agreement with the United States, access to a full hearing for all refugee claimants, and access to basic services including healthcare and housing. We worked with partners and allies on the Allin4Canada public education campaign ahead of the federal election to counter negative attitudes towards refugees. Access and equity for international students was a key advocacy priority last year in response to member agencies’ concerns. As a member of Colour of Poverty – Colour of Change we called for the collection of disaggregated race-based data by all levels of governments to better understand and address the impact of systemic racial inequities. Priorities in Ontario included calling for a minimum wage increase and better protection for workers, legal services for refugees and immigrants and healthcare for all.

### Research

We reviewed the OCASI School for Social Justice (SSJ), a training institute for our member agencies to build capacity in social analysis, community organizing, and advocacy for social and economic justice. The review confirmed the SSJ is a valued and necessary resource for member agencies. However it could not be sustained without reliable and ongoing funding, and the program is on hold for now.

We collaborated with partners and allies on a number of research projects that address OCASI priorities such as the SSHRC partnership Building Migrant Resilience in Cities. We also collaborated on the project, Youth Sexual Health and HIV/STI Prevention in Middle Eastern and North African Communities in Ontario, and several others.

---

### +7,300 Twitter followers

> “Thank you OCASI for your leadership.”
