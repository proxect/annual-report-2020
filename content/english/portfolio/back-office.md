---
title: The Back Office
thumbnail: /images/portfolio/graphic-office.jpg
postImage: /images/portfolio/graphic-office-post.jpg
preview: OCASI’s dedicated administration and technical support team members provide the necessary foundation to support all front-office activities.
service: A strong foundation
date: 2020-07-13T12:00:06.000+06:00
---
## A strong foundation

OCASI’s dedicated administration and technical support team members provide the necessary foundation to support all front-office activities.

### IT and Graphic Design

This year we migrated our websites from Drupal 7 to 8, dedicating hours to successfully solving the many different technical challenges. Drupal 8 allows us to make use of best practice infrastructure and current technology trends for years to come. It lets us strengthen certain features already in use including multilingual components, and infrastructure that allows us to connect our sites to other applications. We made design updates to certain websites, including the Accessibility initiative, to make them more accessible and user-friendly. In-house graphic design expertise has allowed us to be more impactful through the development of visually appealing formats for our resources, and production of social media graphics.

Ongoing activities included monitoring and updating websites, testing applications, and installing and configuring updates for improved security. In addition to the day-to-day technical work and migration activities, we kept abreast of emerging technology trends to be able to leverage them to the maximum extent possible, also strategically plan and assess services that will benefit from innovations in an impactful way.

### Administration

We continued to fulfill administrative functions as efficiently as possible to ensure the smooth running of the organization through financial management and accounting, regulatory compliance, office management and more. With the onset of the pandemic, we continue to migrate most of our administrative activities to an online format.
