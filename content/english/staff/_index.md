---
title: Volunteers, Students, Staff
featureImage: images/single-blog/feature-image.jpg
postImage: images/skill/collage-h.jpg

---
## Behind the scenes

### Direction and Management

#### Executive Director
Debbie Douglas

#### Associate Executive Director
Eta Woldeab

#### Senior Manager, Finance and Administration
Nektarios Kikonyogo**

#### Senior Manager, IT and New Media
John Gilliam**
Alyssa McLeod*
Karen Scott*

#### Senior Manager, Sector Capacity Development
Sajedeh Zahraei

#### Manager, Human Resources
Elaine Takata**

#### Manager, Productive Enterprises
Elena Trapeznikova

### Staff

Sidrah Ahmad*

Habbiba Ahmed

Sihem Ait Hammouda

Lionel Akagah**

Sahar Ashraf

Dena Badawi**

Michelle Ball

Soheil Baouji

Selina Basudde

Mariam Beker

Catherine Belokapov*

Paulina Bermeo

Stefany Brown

Amy Casipullai

Elise Craig**

Joselynn Crosby**

Manolli Ekra

Nira Elgueta**

Gregory Elward*

Anna Finch*

Miyuki Fukuma

Carmen Galvan*

Aini Gauhar*

Peggy Ho

Heffat Hussaini*

Sana Imran*

Sizwe Inkingi

Farheen Khan

Beverly Lawrence

Chenthoori Malankov*

Ali Mathis*

Leona McColeman

Emily Mooney*

Witta Nicoyishakiye*

Chavon Niles

Martha Orellana

Laura Osorio*

Tina Pahlevan*

Kandarp Patel*

Deborah Perne*

Margarita Pintin-Perez**

Theresa Polyakov

Gul-e Rana**

Ila Sethi

Brian Sharpe    

Laura Tachini

Tahira Tasneem*

Floraine Verheyde**

Marcos Vilela


### Intern

Mahmood Khan*


### Students

Haby Bah*

Taskeen Nawab*

Sherene Whyte*





``(*) Left during the year – (**) Joined during the year``
